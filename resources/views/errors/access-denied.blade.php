<!DOCTYPE html>
<html>
<head>
    <title>Complaint Box</title>

    <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    {!! Html::style('css/my-fonts.css') !!}

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #B0BEC5;
            display: table;
            font-weight: 100;
            font-family: 'myMaianFont', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 72px;
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Unauthorized Access.</div>
    </div>


    <div class="text-center">
        <img src="{!! asset('/images/sust_logo.png') !!}" height="250" width="200" alt="the-brains">
        <br>
        <h4> <a class="footertext"> Complaint Box.All Rights Reserved.Copyright<sup>Ⓒ</sup> 2017 </a></h4>
    </div>
</div>
</body>
</html>
