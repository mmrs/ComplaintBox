<!DOCTYPE html>
<html lang="en">
<head>
    <title> Complaint Box</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('fonts/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/checkbox-radio.css') !!}
    {!! Html::style('css/my-fonts.css') !!}
    {!! Html::style('css/my-style.css') !!}

    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/myFunction.js') !!}

    <style>
        .h2-dashboard {
            font-family: myMaianFont;
            color: #c10c05
        }
        .my-Font{
            font-family: myMaianFont;
        }

        .home-page {
            background-image: url({!! asset('/images/complaint.jpg') !!});
            background-size: cover;
            background-attachment: fixed;
            /* Firefox */
            min-height: -moz-calc(100vh - 40px);
            /* WebKit */
            min-height: -webkit-calc(100vh - 40px);
            /* Opera */
            min-height: -o-calc(100vh - 40px);
            /* Standard */
            min-height: calc(100vh - 40px);
        }

        .page-wrapper {
            display: table;
            width: 100%;
            position: relative;
            min-height: -moz-calc(100vh - 40px);
            min-height: -webkit-calc(100vh - 40px);
            min-height: -o-calc(100vh - 40px);
            min-height: calc(100vh - 40px);
        }

        .page-wrapper:before {
            content: "";
            position: absolute;
            width: 100%;
            height: 100%;
            left: 0;
            top: 0;
            background-color: rgba(0,0,0,0.80);
        }

        .valign-center {
            display: table-cell;
            vertical-align: middle;
        }

        .welcome-text {
            color: wheat;
        }

        .welcome-text h2 {
            font-size: 50px;
            margin-top: 50px;
            margin-bottom: 22px;
        }

        .welcome-text p{
             font-size: 28px;
         }


        @media(max-width: 768px) {
            h2, p {
                text-align: center;
            }
            img {
                padding: 100px 100px 0px;
            }
        }



    </style>

</head>

<body>


    @include('layouts.navigation_bar')

    <div class="home-page my-Font">
        <div class="page-wrapper">
            <div class="valign-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <div class="col-md-3">
                                    <img class="img-responsive center-block" src="{!! asset('/images/sust_logo.png') !!}" alt="">
                                </div>
                                <div class="col-md-9">
                                    <div class="welcome-text">
                                        <h2>Welcome!</h2>
                                        <p>Submit Your complaint here anything about your Department. Don't be afraid. You can keep your complaint secret!!</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('committee.modal_confirm_create_committee')
    @include('layouts.footer')

</body>
</html>

