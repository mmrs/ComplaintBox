
{!! Html::style('css/nav-bar.css') !!}
{!! Html::style('css/my-fonts.css') !!}

<nav class="navbar navbar-default  navbar-fixed-top" >
    <div class="container-fluid ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{!! route('home') !!}">Complaint Box</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            @if(!Auth::guest())
                <ul class="nav navbar-nav">

                    @if(Request::is('dashboard'))
                        <li class="active"><a href="{!! route('dashboard') !!}">Dashboard</a></li>
                    @else
                        <li><a href="{!! route('dashboard') !!}">Dashboard</a></li>
                    @endif
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="{!! route('complaintMy') !!}">Complaints <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{!! route('complaintMy') !!}">My Complaints</a></li>
                            <li><a href="{!! route('complaintIndex') !!}">All Complaints</a></li>
                              <li><a href="{!! route('getComplaintSubmit') !!}">Submit New Complaint</a></li>
                            @if(\Illuminate\Support\Facades\Auth::user()->role  != 'USER')
                            <li><a href="{!! route('myCommitteeComplaint') !!}">My Committee Complaints</a></li>
                            @endif
                        </ul>
                    </li>
                    @if(\Illuminate\Support\Facades\Auth::user()->role != 'USER')
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Committees <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{!! route('committeeMy') !!}">My Committees</a></li>
                            <li><a href="{!! route('committeeIndex') !!}">All Committees</a></li>
                            <li><a href="{!! route('myCommitteeComplaint') !!}">My Committee Complaints</a></li>
                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD')
                                <li>
                                    <a data-toggle="modal"
                                            data-target="{{ '#createCommitteeModal' }}" href=""> Create New Committee
                                    </a>
                                </li>
                            @endif
                            @if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD')
                            <li><a href="{!! route('getAssignMember') !!}">Assign Committee Member</a></li>
                            @endif
                        </ul>
                    </li>
                        @endif
                     @if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD')
                    <li><a  href="{!! route('pendingComplaint') !!}">PendingComplaints <sup><span class="badge">{{  \App\Complaint::where('status','PENDING')
                    ->where('department_code','=',\Illuminate\Support\Facades\Auth::user()->department_code)->count()  }}</span> </sup></a></li>
                    @endif
                </ul>
            @endif
            <ul class="nav navbar-nav navbar-right">
                {{--<li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>--}}
                @if(Auth::guest())
                    <li><a href="{!! route('getLogin') !!}"><i class="fa fa-user"></i></span> Login</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <strong>  <i class="fa fa-user"></i> {{ Auth::user()->full_name }}   </strong>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('getLogout') }}"
                                   onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('getLogout') }}" method="get" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
