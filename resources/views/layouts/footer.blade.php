<footer class="footer myCustomFont" style="background: #252933;">

    <div class="container">
        <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
            <font color="#d6dbdd">
                <div class="col-md-3" style="margin-left: 0">
                    Developed By: <a href="http://www.sust.edu/d/cse" target="_blank" style="color:#d6dbdd">Dept. of CSE, SUST</a>
                </div>
                <div class="col-md-6 text-center">
                   Copyright&copy;2016-{{ Date('Y')}} Complaint Box, All rights reserved.
                </div>
                <div class="col-md-3 text-right" style="margin-right: 0">
                    <a data-toggle="modal" data-target="#showDevelopers"  href="" style="color:#d6dbdd">Acknowledgement <!-- <i class="fa fa-heart" aria-hidden="true"></i> --></a>
                </div>
            </font>
        </div>
    </div>

</footer>
<!-- Show Developers -->
<div class="modal fade myCustomFont" id="showDevelopers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="myModalLabel"> <strong> Acknowledgement </strong> </h4>
            </div>

            <div class="modal-body">
                <center>
                    <h4>
                        <p>This project "ComplaintBox" was done as the partial fulfillment of the requirements of the course CSE-446 (Web Engineering Lab).</p> </h4>

                    <p><h4 class="modal-title"> <strong>By</strong> </h4>
                    Md. Moshiur Rahman<br>
                    <a href="mailto:moshiur.siyam@gmail.com?Subject=ComplaintBox" target="_top">moshiur.siyam@gmail.com </a><br> CSE'12, SUST. <br>
                    && <br>
                    M. Tahmid Hossain <br>
                    <a href="mailto:tahmidolee@gmail.com?Subject=ComplaintBox" target="_top">tahmidolee@gmail.com </a><br> CSE'12, SUST.</p>

                    <P>
                    <h4 class="modal-title"><strong> Under Supervision of</strong></h4>
                    Md Masum<br>
                    <a href="mailto:masum-cse@sust.edu?Subject=ComplaintBox" target="_top">masum-cse@sust.edu</a><br>
                    Associate Professor, CSE, SUST.</p>
                </center>
            </div>
            <div class="modal-footer">

                <a href="#" class="btn btn-info" data-dismiss="modal" >Close</a>
            </div>
        </div>
    </div>
</div>