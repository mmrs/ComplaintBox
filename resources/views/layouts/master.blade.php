<!DOCTYPE html>
<html lang="en">
<head>
    <title> Complaint Box</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('fonts/font-awesome/css/font-awesome.css') !!}
    {!! Html::style('css/checkbox-radio.css') !!}
    {!! Html::style('css/my-fonts.css') !!}
    {!! Html::style('css/my-style.css') !!}

    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/myFunction.js') !!}

</head>

<body style="">
@include('layouts.navigation_bar')
<div class= "page-content" style="padding-top: 60px; padding-bottom: 20px;">
@yield('content')
</div>
@include('committee.modal_confirm_create_committee')
@include('layouts.footer')
</body>

</html>
