@extends('layouts.master')



@section('content')
    <div class="register-body">
        <div class="container submitForm">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                {{ Form::open(array('route' =>  'postComplaintSubmit',
                                                'method' => 'post',
                                                'class' => 'form-submit-complaint',
                                                'name' => 'myForm'
                                                )) }}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h1 class="form-signin-heading myCustomFont text-center">Submit New Complaint</h1>
                <hr>
                @include('includes.alert')
                <div class="login-wrap">
                    {{ Form::label('complaint_title', 'Complaint Title*', array('' => ''))}}
                    {{ Form::text('title', '', array('class' => 'form-control', 'autofocus','required','placeholder'=>'Complaint Title','name'=>'title')) }}

                    {{ Form::label('concerned_department', 'Concerned Department*', array('' => '')) }}
                    {{ Form::select('department_code', $department, null, array('class' => 'form-control')) }}

                    {{ Form::label('category', 'Complaint Category*', array('' => '')) }}
                    {{ Form::select('category', $category, null, array('class' => 'form-control')) }}
                    {{ Form::label('privacy', 'Privacy*', array('' => '')) }}

                    {{--{{ Form::select('privacy', ['PRIVATE'=>'PRIVATE','PUBLIC'=>'PUBLIC'], null, array('class' => 'form-control')) }}--}}


                    <div class="radio radio-danger">
                    {{ Form::radio('privacy','PRIVATE') }}
                    {{ Form::label('privacy', 'PRIVATE', array('class' => 'myCustomFont')) }}
                        <br>
                    {{ Form::radio('privacy', 'PUBLIC',true) }}
                    {{ Form::label('privacy', 'PUBLIC', array('class' => 'myCustomFont')) }}
                    </div>

                    {{ Form::label('description', 'Complaint Details*', array('' => '')) }}
                    {{ Form::textarea('description', '', array('class' => 'form-control', 'autofocus','required','placeholder'=>'Complaint Details....','name'=>'description')) }}

                    *all fields are mandatory
                    <br><br><br>
                    <div class="col-lg-4 col-lg-offset-4">
                        {{ Form::submit('Submit', array('class' => 'btn btn-block btn-login btn-primary confirm-alert')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
        </br>
    </div>

    {!! Html::script('/js/bootbox.min.js') !!}

    <script>
        $(document).on("click", ".confirm-alert", function(e) {
            e.preventDefault();
            if(!validateForm())
                return;
            bootbox.confirm("Are you sure to submit this Complaint?", function(result) {
                if (result) {
                    console.log("User confirmed dialog");
                    var x = document.getElementsByClassName("form-submit-complaint");
                    x[0].submit(); // Form submission
                } else {
                    console.log("User declined dialog");
                }
            });
        });

        function validateForm() {
            var x = document.forms["myForm"]["title"].value;
            var y = document.forms["myForm"]["description"].value;
            if (x == "" || y=="") {
                bootbox.alert("Fill all the fields correctly. All fields are mandatory.");
                return false;
            }
            return true;
        }
    </script>
@stop