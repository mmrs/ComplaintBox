@extends('layouts.master')
@include('includes.alert')
@section('content')


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="container myCustomFont">
        <h2 class="text-center">{{ $page_title }}</h2>
        {{--<hr>--}}
        <div class="table table-responsive">
            <table class="display table table-bordered table-striped" id="example">
                <thead>
                <tr>
                    {{--<th class="success">USER ID</th>--}}
                    <th class="success">USER DEPT.</th>
                    <th class="success">COMPLAINT TITLE</th>
                    <th class="success">SUBMITTED ON</th>
                    <th class="success">STATUS</th>
                    {{--<th class="danger">STATUS</th>--}}
                </tr>
                </thead>
                <tbody>

                @foreach($complaints as $index=>$complaint)


                    <td>{{ $complaint->user->department_code }}</td>
                    <td>
                        <a href="{{ route('showSingleComplaint',$complaint->id) }}" id="link">
                            <div style="height:100%;width:100%">
                                {{  $complaint->title }}
                            </div>
                        </a>
                    </td>
                    <td><strong>  {{ $complaint->created_at->format('jS F Y, h:i A') }}     </strong></td>
                    <td><strong>   {{ $complaint->status }}   </strong></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <!-- copy start-->
    {!! Html::style('assets/data-tables/DT_bootstrap.css') !!}
    {!! Html::script('assets/data-tables/jquery.dataTables.js') !!}
    {!! Html::script('assets/data-tables/DT_bootstrap.js') !!}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
@endsection
