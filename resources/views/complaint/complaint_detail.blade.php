@extends('layouts.master')

@section('content')

    <div class="container" style="width: 100%">

        @if($complaint->status == "CLOSED")
            <div class="panel panel-success col-lg-8 col-lg-offset-2" style="padding: 0">
                @elseif($complaint->status == "REJECTED")
                    <div class="panel panel-danger col-lg-8 col-lg-offset-2" style="padding: 0">
                        @elseif($complaint->status == "PENDING")
                            <div class="panel panel-warning col-lg-8 col-lg-offset-2" style="padding: 0">
                                @elseif($complaint->status == "IN-PROGRESS")
                                    <div class="panel panel-info col-lg-8 col-lg-offset-2" style="padding: 0">
                                        @else
                                            <div class="panel panel-default col-lg-8 col-lg-offset-2"
                                                 style="padding: 0">
                                                @endif
                                                @include('includes.alert')
                                                <div class="panel-heading text-center">
                                                    <div class="myCustomFont ">
                                                        <h2 class="text-center myCustomFont">{{ $complaint->title }}</h2>
                                                        <h4 class="text-center myCustomFont"><label for="name"> <i
                                                                        class="fa fa-user"></i>
                                                            </label>
                                                            <a href="" data-toggle="tooltip" data-placement="bottom"
                                                               title="{{ "Dept: "  . $user->department_code  }} "> {{ $user->full_name }} </a>
                                                            {{--<label for="dept>"> Dept--}}
                                                            {{--: </label>
                                                        {{--<label for="reg"> Reg. No--}}
                                                            {{--: </label> {{ $user->registration_no}}<br>--}}
                                                            <label for="submitted"> <i class="fa fa-clock-o"></i>
                                                            </label> {{ $complaint->created_at->format('jS F Y, h:i A') }}
                                                            <br>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <ul class="nav">
                                                        <li class="well">
                                                            <label for="status"> Category
                                                                : </label> {{ $category->display_name }}
                                                            <br>
                                                            <label for="privacy"> Privacy
                                                                : </label> {{ $complaint->privacy}}
                                                            <label for="status"> Status : </label>
                                                            <strong> {{ $complaint->status}} </strong> <br>
                                                            <label for="description"> Description
                                                                : </label> {{$complaint->description }}</li>


                                                        @if((\Illuminate\Support\Facades\Auth::user()->role == "HEAD"||
                                                        (\Illuminate\Support\Facades\Auth::user()->role == "COMMITTEE" && $complaint->privacy == "PUBLIC")
                                                        || $isSubmitReport == "true") &&
                                                          ($complaint->status =="IN-PROGRESS" || $complaint->status =="CLOSED"))
                                                            <ul class="well">
                                                                @foreach($assigned as $index)
                                                                    <label for="title">Committe Name
                                                                        :</label> {{ $index['title'] }}
                                                                    <label for="members"> <i class="fa fa-users"></i>
                                                                    </label> {{ $index['names'] }}
                                                                    <br>
                                                                @endforeach
                                                            </ul>
                                                        @endif
                                                        @if((\Illuminate\Support\Facades\Auth::user()->role == "HEAD" || $isSubmitReport == "true") &&
                                                          ($complaint->status =="IN-PROGRESS" || $complaint->status =="CLOSED"))
                                                            <ul class="well">
                                                                <label for="comments"> Comments :</label>
                                                                <br>
                                                                @foreach($complaint->reports as $report)
                                                                    <span> " {{ $report->comment }} "</span>
                                                                    <br>
                                                                @endforeach
                                                            </ul>
                                                        @endif

                                                        <div class="btn-group text-center myCustomFont">

                                                            @if(\App\UserPenalize::select('penalize_date')
                                                            ->where('user_id', '=', $user->id)->max('penalize_date') > \Carbon\Carbon::now())

                                                            @elseif(($isSubmitReport || \Illuminate\Support\Facades\Auth::user()->role == "HEAD")
                                                             && $complaint->status!="CANCELLED" && $complaint->status!="CLOSED")
                                                                <a class="btn btn-warning confirm-alert"
                                                                   style="margin-right: 15px; margin-top: 5px;"
                                                                   href="{!! route("penalizeUser", ["user"=>$user->id,"complaint"=>$complaint->id]) !!}">
                                                                    <i class="fa fa-lock"></i> Penalize User</a>
                                                            @endif

                                                            @if(\Illuminate\Support\Facades\Auth::user()->role == "HEAD" && $complaint->status == "PENDING")
                                                                <a href="{!!  route('rejectComplaint', ['id' => $complaint->id]) !!} "
                                                                   style="margin-right: 15px;margin-top: 5px;"
                                                                   class="btn btn-danger">
                                                                    <i class="fa fa-close"></i> Reject Complaint</a>
                                                            @endif

                                                            @if(\Illuminate\Support\Facades\Auth::user()->role == "HEAD" && $complaint->status !="REJECTED"
                                                                && $complaint->status !="CLOSED" && $complaint->status!="CANCELLED"  )
                                                                @include('committee.modal_assign_committee')
                                                                <a class="btn btn-primary" style="margin-right: 15px;margin-top: 5px;"
                                                                   data-toggle="modal"
                                                                   data-target="{{ '#assignCommitteeModal' }}" href="">
                                                                    <i class="fa fa-dedent"></i>
                                                                    Assign Committee
                                                                </a>
                                                            @endif

                                                            @if($isSubmitReport && $complaint->status !="REJECTED"
                                                                 && $complaint->status!="CANCELLED")
                                                                <a class="btn btn-success" style="margin-right: 15px;margin-top: 5px;"
                                                                   href="
                                                            {{   route('getSubmitReport',['complaint'=>$complaint->id])  }}"
                                                                   role="button"> <i class="fa fa-comment"></i> Submit
                                                                    Report </a>
                                                            @endif
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                            <br>
                                            <button class="btn btn-info col-lg-1 col-lg-offset-5"
                                                    onclick="goBack()">
                                                <i class="fa fa-backward"></i> Go Back
                                            </button>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>


    {{ Html::style('/fonts/font-awesome/css/font-awesome.css') }}
    {{--bootbox confirm dialog--}}
    {{ Html::script('/js/bootbox.min.js') }}

    <script>
        $(document).on("click", ".confirm-alert", function (e) {
            e.preventDefault();
            var eventObject = this;
            bootbox.confirm("This user will not be able to submit another complaint in next 15 days. Are you sure to Perform this Operation?", function (result) {
                if (result) {
                    console.log("User confirmed dialog");
                    window.location = eventObject.href;

                } else {
                    console.log("User declined dialog");
                }
            });
        });
    </script>

@endsection
