@extends('layouts.master')
@section('content')

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <div class="container myCustomFont">
        <h2 class="text-center">My Complaints</h2>

        @include('includes.alert')
        <div class="table-responsive">
            <table class="display table table-bordered table-striped table-responsive" id="example">
                <thead>
                <tr>
                    <th class="success">DEPT.</th>
                    <th class="success">COMPLAINT TITLE</th>
                    <th class="success">SUBMITTED ON</th>
                    <th class="success">PRIVACY</th>
                    <th class="success">STATUS</th>
                    <th class="success">ACTION</th>
                </tr>
                </thead>
                <tbody>

                @foreach($complaints as $complaint)
                    <tr>
                        <td>{{ $complaint->department_code }}</td>
                        <td>
                            <a href="{{ route('showSingleComplaint',$complaint->id) }}" id="link">
                                <div style="height:100%;width:100%">
                                    {{  $complaint->title }}
                                </div>
                            </a>
                        </td>
                        <td><strong>   {{ $complaint->created_at->format('jS F Y, h:i A') }}   </strong></td>
                        <td><strong>   {{ $complaint->privacy }}          </strong></td>
                        <td><strong>   {{ $complaint->status }}          </strong></td>
                        <td>
                            @if($complaint->status == "PENDING")
                                <a class="btn btn-danger btn-block"
                                   href="{!! route("cancelComplaint", $complaint->id) !!}"> <i class="fa fa-remove"></i> CANCEL</a>
                            @else
                                <strong><h5 class="text-center"> NONE</h5></strong>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <!-- copy start-->
    {{ Html::style('assets/data-tables/DT_bootstrap.css') }}
    {{ Html::script('js/jquery.js') }}
    {{ Html::script('assets/data-tables/jquery.dataTables.js') }}
    {{ Html::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->

@endsection
