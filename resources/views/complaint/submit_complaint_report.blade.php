@extends('layouts.master')

<style>
    .my-Font{
        font-family: myMaianFont;
        color: #c10c05
    }


</style>

@section('content')

        <div class="container">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                {{ Form::open(array('route' =>  'postSubmitReport',
                                                'method' => 'post',
                                                'class' => 'form-submit-report'
                                                )) }}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3 class="form-signin-heading myCustomFont text-center">Submit Complaint Report</h3>
                <hr>
                <h2 class="my-Font text-center">  Complaint title: "{{ $complaint->title }} "</h2>
                @include('includes.alert')
                <div class="login-wrap">

                    {{--{{ Form::label('committee_title', 'Committee Title*', array('' => '')) }} --}}
                    {{ Form::hidden('complaintId',$complaintId, null, array('class' => 'form-control')) }}

                    {{--{{ Form::label('user_name', 'User Name*', array('' => '')) }}--}}
                    {{ Form::hidden('userId',$userId, null, array('class' => 'form-control')) }}
                    {{ Form::label('', '', array('' => '')) }}
                    {{ Form::textarea('reportBody', '', array('class' => 'form-control', 'autofocus','required')) }}

                    <br>
                    <div class="text-center col-lg-6 col-lg-offset-3">
                        <a class="btn btn-warning" href="" onclick="goBack()"> <i class="fa fa-backward"></i> Back  </a>
                        {{ Form::submit('Submit', array('class' => 'btn btn-login btn-primary')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>

        </div>

@stop