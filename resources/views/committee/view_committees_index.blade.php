@extends('layouts.master')
@section('content')

    {{--@include('committee.modal_confirm_create_committee')--}}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="container myCustomFont">
        <h2 class="text-center">All Committees</h2>
        {{--<hr>--}}
        {{--<a class = "btn btn-danger block" href = "submit" role = "button">Create New Committee</a>--}}
        {{--<button type="button" class="btn btn-danger block" data-toggle="modal"--}}
        {{--data-target="{{ '#createCommitteeModal' }}"> Create New Committee--}}
        {{--</button>--}}

        <div class="table-responsive">

        @include('includes.alert')

        <table class="display table table-bordered table-striped text-center" id="example">
            <thead>
            <tr>
                <th class="success text-center">MEMBERS</th>
                <th class="success text-center">TITLE</th>
                <th class="success text-center">CREATED ON</th>
                <th class="success text-center">STATUS</th>
                @if(\Illuminate\Support\Facades\Auth::user()->role=="HEAD")
                    <th class="success text-center"> ACTION</th> @endif
            </tr>
            </thead>

            <tbody>

            @foreach($committees as $committee)

                <tr>
                    {{--<td> <button type="button" class="btn btn-info btn-link" data-toggle="modal"--}}
                    {{--data-target="{{ '#modal-'.$complaint->id }}">{{ $complaint->user->login_name }}</button> </td>--}}
                    <td>
                        <a href="{{ route('showCommittee',$committee['id']) }}">
                            <div style="height:100%;width:100%">
                                @foreach($committee['members'] as $member)
                                    <span> {!! $member !!} </span>
                                    <br>
                                @endforeach
                            </div>
                        </a>
                    </td>

                    <td>
                        <a href="{{ route('showCommittee',$committee['id']) }}">
                            <div style="height:100%;width:100%">
                                {{  $committee['title'] }}
                            </div>
                        </a>
                    </td>
                    <td><strong>  {{ $committee['created_at']->format('jS F, Y') }}  </strong></td>
                    <td><strong>   {{ $committee['status'] }}          </strong></td>
                    @if(\Illuminate\Support\Facades\Auth::user()->role=="HEAD")
                        <td>
                            @if($committee['status'] == "OPEN")
                                <a class="btn btn-warning confirm-alert"
                                   href="{!! route("closeCommittee", $committee['id']) !!}"> <i class="fa fa-lock"></i>
                                    CLOSE</a>
                            @else
                                <a class="btn btn-success confirm-alert"
                                   href="{!! route("openCommittee", $committee['id']) !!}"> <i class="fa fa-unlock"></i>
                                    OPEN</a>
                            @endif
                            <a class="btn btn-danger confirm-alert"
                               href="{!! route("deleteCommittee", $committee['id']) !!}"> <i class="fa fa-remove"></i>
                                DELETE</a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
    {{--bootbox confirm dialog--}}

    {!! Html::script('/js/bootbox.min.js') !!}

    <script>
        $(document).on("click", ".confirm-alert", function (e) {
            e.preventDefault();
            var eventObject = this;
            bootbox.confirm("Are you sure to Perform this Operation?", function (result) {
                if (result) {
                    console.log("User confirmed dialog");
                    window.location = eventObject.href;

                } else {
                    console.log("User declined dialog");
                }
            });
        });
    </script>

    <!-- copy start-->
    {{ Html::style('assets/data-tables/DT_bootstrap.css') }}
    {{ Html::script('js/jquery.js') }}
    {{ Html::script('assets/data-tables/jquery.dataTables.js') }}
    {{ Html::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
@endsection
