<!-- Modal -->
<div class="modal fade" id="createCommitteeModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title myCustomFont">Give Committee Title</h4>
            </div>
            <div class="modal-body">
                <form class="information">
                    {{  csrf_field()  }}
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" id="title" required>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="create">Create</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
//twitter bootstrap script
        $("#create").click(function () {
            $.ajax({
                type: "get",
                url: "{!! URL::route("newCommittee") !!}",
                data: "title=" + $('#title').val(),
                success: function (data) {
//                        alert(data.msg);
//                        location.reload();
                    window.location = "{!! URL::route("getAssignMember") !!}";
                },
                error: function (data) {
                    alert(data.msg);
                }
            });
        });
    })
</script>
