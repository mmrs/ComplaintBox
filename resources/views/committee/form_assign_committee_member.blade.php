@extends('layouts.master')


@section('content')
    <div class="register-body">
        <div class="container">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                {{ Form::open(array('route' =>  'postAssignMember',
                                                'method' => 'post',
                                                'class' => 'form-signin'
                                                )) }}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h2 class="form-signin-heading myCustomFont text-center">Assign Member to Committee</h2>
                <hr>
                @include('includes.alert')
                <div class="login-wrap">

                    <br>
                    {{ Form::label('committee_title', 'Committee Title*', array('' => '')) }}
                    {{ Form::select('committee_id', $committees, null, array('class' => 'form-control myCustomFont')) }}

                    {{--{{ Form::label('user_name', 'User Name*', array('' => '')) }}--}}
                    {{--{{ Form::select('user_id', $users, null, array('class' => 'form-control')) }}--}}
                    <h3 class="myCustomFont">Select Members For This Committee</h3>
                    @foreach($users as $user)

                        <div class="checkbox checkbox-success myCustomFont">
                            {{ Form::checkbox('user_id[]', $user->id, null, ['class' => 'field']) }}
                            {{ Form::label('user_name', $user->full_name, array('class' => 'myCustomFont')) }}<br>
                        </div>


                    @endforeach

                    <br><br>
                    <div class="text-center col-lg-6 col-lg-offset-3 myCustomFont">
                        <a class="btn btn-warning" href="" onclick="goBack()"> <i class="fa fa-backward"></i> Back</a>
                    {{ Form::submit('Assign Member', array('class' => 'btn btn-login btn-primary')) }}

                    </div>
                </div>
                {{ Form::close() }}
            </div>

        </div>
    </div>

@stop
