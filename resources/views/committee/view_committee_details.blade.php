@extends('layouts.master')
@include('includes.alert')

{{--<style>--}}

{{--.well{--}}

{{--padding: 0;--}}
{{--margin: 0;--}}
{{--}--}}
{{--</style>--}}

@section('content')
    <div class="container">
        <div class="container-fluid">
            <br><br>
            @include('includes.alert')
            <div class="panel-group">
                <div class="row">
                    @if($committee->status == "OPEN")
                        <div class="panel panel-success col-lg-8 col-lg-offset-2" style="padding: 0">
                            @else
                                <div class="panel panel-danger col-lg-8 col-lg-offset-2" style="padding: 0">
                                    @endif
                                    <div class="panel-heading text-center">
                                        <h1 class="text-center myCustomFont">{{ $committee->title  }}</h1>
                                        <h4 class="myCustomFont">
                                            <label for="status"> Department : </label> {{ $committee->department_code }}
                                            <label for="status"> Status : </label> {{ $committee->status }}<br>
                                            <label for="status"><i class="fa fa-clock-o"></i>
                                            </label> {{ $committee->created_at->format('jS F Y, h:i A') }}
                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="nav">
                                            <h2 class="myCustomFont">Members:</h2>
                                            @foreach($committee->members as $member)
                                                <li class="well"><label for="description"> User Name
                                                        : </label> {{$member->user->login_name }}
                                                    <br>
                                                    <label for="description"> Full Name
                                                        : </label> {{$member->user->full_name }} <br>
                                                    <label for="description"> Designation
                                                        : </label> {{$member->user->designation }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                        </div>
                        <br>
                        <button class="btn btn-info col-lg-1 col-lg-offset-5"
                                onclick="goBack()"><i class="fa fa-backward"></i> Go Back
                        </button>
                </div>
            </div>
        </div>
        <br><br>
    </div>
  
@endsection
