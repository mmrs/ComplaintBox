@extends('layouts.master')
@section('content')


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="container myCustomFont">
        <h2 class="text-center">My Committees</h2>
        {{--<hr>--}}
        <div class="table-responsive">
        <table class="display table table-bordered table-striped" id="example">
            <thead>
            <tr>
                {{--<th class="success">ID</th>--}}
                <th class="success">TITLE</th>
                <th class="success">CREATED ON</th>
                <th class="success">STATUS</th>
                {{--<th class="danger">STATUS</th>--}}
            </tr>
            </thead>

            <tbody>

            @foreach($committees as $index=>$committee)

                <tr id='tr' onclick="myFunction(this)">
                    {{--<td> <button type="button" class="btn btn-info btn-link" data-toggle="modal"--}}
                    {{--data-target="{{ '#modal-'.$complaint->id }}">{{ $complaint->user->login_name }}</button> </td>--}}
                    {{--<td >--}}
                    {{--<a href= "{{ route('showCommittee',$committee->id) }}"  >--}}
                    {{--<div style="height:100%;width:100%">--}}
                    {{--{{  $committee->id }}--}}
                    {{--</div>--}}
                    {{--</a>--}}
                    {{--</td>--}}

                    <td>
                        <a href="{{ route('showCommittee',$committee->id) }}">
                            <div style="height:100%;width:100%">
                                {{  $committee->title }}
                            </div>
                        </a>
                    </td>
                    <td><strong>  {{ date('F d, Y', strtotime($committee->created_at)) }}    </strong></td>
                    <td><strong>   {{ $committee->status }}          </strong></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>




    <!-- copy start-->
    {{ Html::style('assets/data-tables/DT_bootstrap.css') }}
    {{ Html::script('js/jquery.js') }}
    {{ Html::script('assets/data-tables/jquery.dataTables.js') }}
    {{ Html::script('assets/data-tables/DT_bootstrap.js') }}

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
@endsection