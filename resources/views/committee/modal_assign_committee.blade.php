<!-- Modal -->
<div class="modal fade" id="assignCommitteeModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content myCustomFont">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title myCustomFont">Select Committee</h4>
            </div>
            <div class="modal-body">
                <ul class="nav">
                    {{ Form::open(array('route' =>  'postAssignCommittee',
                               'method' => 'post',
                               'class' => 'form-signin'
                               )) }}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="login-wrap">
                        {{--{{ Form::label('committee_title', 'Complaint ID*', array('' => '')) }}--}}
                        {{ Form::hidden('complaint_id',$complaint->id, null, array('class' => 'form-control')) }}

                        {{ Form::label('committee_title', 'Committee Title', array('' => '')) }}
                        {{ Form::select('committee_id', $committees, null, array('class' => 'form-control')) }}

                        <br>
                        {{ Form::submit('Assign', array('class' => 'btn btn-login btn-info col-lg-2 col-lg-offset-5')) }}
                    </div>
                    {{ Form::close() }}
                </ul>

            </div>
            {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-success" id="create">Create</button>--}}
                {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
            {{--</div>--}}
        </div>
    </div>
</div>
