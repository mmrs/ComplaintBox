-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2017 at 08:16 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complaint_box`
--
CREATE DATABASE IF NOT EXISTS `complaint_box` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `complaint_box`;

-- --------------------------------------------------------

--
-- Table structure for table `assigned_committee`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `assigned_committee`;
CREATE TABLE `assigned_committee` (
  `id` int(10) UNSIGNED NOT NULL,
  `complaint_id` int(10) UNSIGNED NOT NULL,
  `committee_id` int(10) UNSIGNED NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `assigned_committee`:
--   `committee_id`
--       `committees` -> `id`
--   `complaint_id`
--       `complaints` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `committees`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `committees`;
CREATE TABLE `committees` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'OPEN',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `committees`:
--

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--
-- Creation: May 14, 2017 at 06:07 AM
--

DROP TABLE IF EXISTS `complaints`;
CREATE TABLE `complaints` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `privacy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `complaints`:
--   `category_id`
--       `complaint_categories` -> `id`
--   `user_id`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `complaint_categories`
--
-- Creation: May 14, 2017 at 06:07 AM
--

DROP TABLE IF EXISTS `complaint_categories`;
CREATE TABLE `complaint_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `complaint_categories`:
--

--
-- Dumping data for table `complaint_categories`
--

INSERT INTO `complaint_categories` (`id`, `category_name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'category1', 'Class Related Issues', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(2, 'category2', 'Lab Related Issues', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(3, 'category3', 'Office Employee Related Issues', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(4, 'category4', 'Teacher Related Issues', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(5, 'category5', 'Other Issues', '2017-05-14 06:14:52', '2017-05-14 06:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `departments`:
--

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_code`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'CSE', 'Computer Science & Engineering', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(2, 'EEE', 'Electrical & Electronic Engineering', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(3, 'IPE', 'Industrial & Production Engineering', '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(4, 'PHY', 'Physics', '2017-05-14 06:14:52', '2017-05-14 06:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `member_committee`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `member_committee`;
CREATE TABLE `member_committee` (
  `id` int(10) UNSIGNED NOT NULL,
  `committee_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `member_committee`:
--   `committee_id`
--       `committees` -> `id`
--   `user_id`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `migrations`:
--

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_10_08_141224_create_complaint_category_table', 1),
(4, '2016_10_09_150728_create_department_table', 1),
(5, '2016_10_09_162432_create_complaint_table', 1),
(6, '2016_10_09_162541_create_user_penalization_table', 1),
(7, '2016_10_09_162744_create_committee_table', 1),
(8, '2016_10_09_162805_create_member_committee_table', 1),
(9, '2016_10_09_162844_create_assigned_committee_table', 1),
(10, '2016_10_09_162947_create_report_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `password_resets`:
--

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `reports`;
CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `complaint_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `reports`:
--   `complaint_id`
--       `complaints` -> `id`
--   `user_id`
--       `users` -> `id`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--
-- Creation: May 14, 2017 at 06:07 AM
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `designation` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registration_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `users`:
--

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login_name`, `full_name`, `department_code`, `designation`, `registration_no`, `role`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mmrs', 'Md. Moshiur Rahman', 'CSE', 'Student', '2012331049', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(2, 'olee', 'M. Tahmid Hossain', 'CSE', 'student', '2012331017', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(3, 'shopon', 'Md. Habibur Rahman', 'CSE', 'student', '2012331029', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(4, 'sharif', 'Sharif Siddique', 'CSE', 'student', '2012331025', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(5, 'akash', 'Saumik Kumar Saha', 'EEE', 'student', '2012345012', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(6, 'abdullah', 'Md. Abdullah', 'IPE', 'student', '2012325014', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(7, 'rumie', 'Rumie Miraj Ul Amin', 'PHY', 'student', '2012145019', 'USER', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(8, 'headcse', 'Dr Mohammad Reza Selim', 'CSE', 'Professor', NULL, 'HEAD', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(9, 'headeee', 'Dr Mohammad Shahidur Rahman', 'EEE', 'Professor', NULL, 'HEAD', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(10, 'headipe', 'Dr Mohammad Muhshin Aziz Khan', 'IPE', 'Professor', NULL, 'HEAD', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(11, 'headphy', 'Dr. Mr. Head of PHY', 'PHY', 'Professor', NULL, 'HEAD', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(12, 'teachercse3', 'Md. Saiful Islam', 'CSE', 'Assistant Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(13, 'teachercse2', 'Sabir Ismail', 'CSE', 'Assistant Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(14, 'teachercse1', 'Md Masum', 'CSE', 'Associate Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(15, 'teachercse4', 'Biswapriyo Chakrabarty', 'CSE', 'Lecturer', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(16, 'teachereee1', 'Tuhin Dey', 'EEE', 'Assistant Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(17, 'teachereee2', 'Biswajit Paul', 'EEE', 'Assistant Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(18, 'teachereee3', 'Mohammad Kamruzzaman Khan Prince', 'EEE', 'Lecturer', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(19, 'teacherphy1', 'Dr. Md. Shah Alam', 'PHY', 'Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52'),
(20, 'teacheripe1', 'Engr A B M Abdul Malek', 'IPE', 'Associate Professor', NULL, 'COMMITTEE', 'aaaaaaaa', NULL, '2017-05-14 06:14:52', '2017-05-14 06:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `user_penalizes`
--
-- Creation: May 14, 2017 at 06:14 AM
--

DROP TABLE IF EXISTS `user_penalizes`;
CREATE TABLE `user_penalizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `complaint_id` int(10) UNSIGNED NOT NULL,
  `department_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `penalize_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- RELATIONS FOR TABLE `user_penalizes`:
--   `complaint_id`
--       `complaints` -> `id`
--   `user_id`
--       `users` -> `id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assigned_committee`
--
ALTER TABLE `assigned_committee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `assigned_committee_complaint_id_committee_id_unique` (`complaint_id`,`committee_id`),
  ADD KEY `assigned_committee_committee_id_foreign` (`committee_id`);

--
-- Indexes for table `committees`
--
ALTER TABLE `committees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`),
  ADD KEY `complaints_user_id_foreign` (`user_id`),
  ADD KEY `complaints_category_id_foreign` (`category_id`);

--
-- Indexes for table `complaint_categories`
--
ALTER TABLE `complaint_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `complaint_categories_category_name_unique` (`category_name`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departments_department_code_unique` (`department_code`);

--
-- Indexes for table `member_committee`
--
ALTER TABLE `member_committee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `member_committee_committee_id_user_id_unique` (`committee_id`,`user_id`),
  ADD KEY `member_committee_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reports_user_id_foreign` (`user_id`),
  ADD KEY `reports_complaint_id_foreign` (`complaint_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_login_name_unique` (`login_name`);

--
-- Indexes for table `user_penalizes`
--
ALTER TABLE `user_penalizes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_penalizes_user_id_complaint_id_unique` (`user_id`,`complaint_id`),
  ADD KEY `user_penalizes_complaint_id_foreign` (`complaint_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assigned_committee`
--
ALTER TABLE `assigned_committee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `committees`
--
ALTER TABLE `committees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `complaint_categories`
--
ALTER TABLE `complaint_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `member_committee`
--
ALTER TABLE `member_committee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user_penalizes`
--
ALTER TABLE `user_penalizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `assigned_committee`
--
ALTER TABLE `assigned_committee`
  ADD CONSTRAINT `assigned_committee_committee_id_foreign` FOREIGN KEY (`committee_id`) REFERENCES `committees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `assigned_committee_complaint_id_foreign` FOREIGN KEY (`complaint_id`) REFERENCES `complaints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `complaints`
--
ALTER TABLE `complaints`
  ADD CONSTRAINT `complaints_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `complaint_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `complaints_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `member_committee`
--
ALTER TABLE `member_committee`
  ADD CONSTRAINT `member_committee_committee_id_foreign` FOREIGN KEY (`committee_id`) REFERENCES `committees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `member_committee_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reports`
--
ALTER TABLE `reports`
  ADD CONSTRAINT `reports_complaint_id_foreign` FOREIGN KEY (`complaint_id`) REFERENCES `complaints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_penalizes`
--
ALTER TABLE `user_penalizes`
  ADD CONSTRAINT `user_penalizes_complaint_id_foreign` FOREIGN KEY (`complaint_id`) REFERENCES `complaints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_penalizes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Metadata
--
USE `phpmyadmin`;

--
-- Metadata for assigned_committee
--

--
-- Metadata for committees
--

--
-- Metadata for complaints
--

--
-- Metadata for complaint_categories
--

--
-- Metadata for departments
--

--
-- Metadata for member_committee
--

--
-- Metadata for migrations
--

--
-- Metadata for password_resets
--

--
-- Metadata for reports
--

--
-- Metadata for users
--

--
-- Metadata for user_penalizes
--

--
-- Metadata for complaint_box
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
