
<?php echo Html::style('css/nav-bar.css'); ?>

<?php echo Html::style('css/my-fonts.css'); ?>


<nav class="navbar navbar-default  navbar-fixed-top" >
    <div class="container-fluid ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo route('home'); ?>">Complaint Box</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <?php if(!Auth::guest()): ?>
                <ul class="nav navbar-nav">

                    <?php if(Request::is('dashboard')): ?>
                        <li class="active"><a href="<?php echo route('dashboard'); ?>">Dashboard</a></li>
                    <?php else: ?>
                        <li><a href="<?php echo route('dashboard'); ?>">Dashboard</a></li>
                    <?php endif; ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo route('complaintMy'); ?>">Complaints <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo route('complaintMy'); ?>">My Complaints</a></li>
                            <li><a href="<?php echo route('complaintIndex'); ?>">All Complaints</a></li>
                              <li><a href="<?php echo route('getComplaintSubmit'); ?>">Submit New Complaint</a></li>
                            <?php if(\Illuminate\Support\Facades\Auth::user()->role  != 'USER'): ?>
                            <li><a href="<?php echo route('myCommitteeComplaint'); ?>">My Committee Complaints</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php if(\Illuminate\Support\Facades\Auth::user()->role != 'USER'): ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Committees <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo route('committeeMy'); ?>">My Committees</a></li>
                            <li><a href="<?php echo route('committeeIndex'); ?>">All Committees</a></li>
                            <li><a href="<?php echo route('myCommitteeComplaint'); ?>">My Committee Complaints</a></li>
                            <?php if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD'): ?>
                                <li>
                                    <a data-toggle="modal"
                                            data-target="<?php echo e('#createCommitteeModal'); ?>" href=""> Create New Committee
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD'): ?>
                            <li><a href="<?php echo route('getAssignMember'); ?>">Assign Committee Member</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                        <?php endif; ?>
                     <?php if(\Illuminate\Support\Facades\Auth::user()->role == 'HEAD'): ?>
                    <li><a  href="<?php echo route('pendingComplaint'); ?>">PendingComplaints <sup><span class="badge"><?php echo e(\App\Complaint::where('status','PENDING')
                    ->where('department_code','=',\Illuminate\Support\Facades\Auth::user()->department_code)->count()); ?></span> </sup></a></li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
            <ul class="nav navbar-nav navbar-right">
                
                <?php if(Auth::guest()): ?>
                    <li><a href="<?php echo route('getLogin'); ?>"><i class="fa fa-user"></i></span> Login</a></li>
                <?php else: ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <strong>  <i class="fa fa-user"></i> <?php echo e(Auth::user()->full_name); ?>   </strong>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?php echo e(route('getLogout')); ?>"
                                   onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="<?php echo e(route('getLogout')); ?>" method="get" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </li>
                        </ul>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>
