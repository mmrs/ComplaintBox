<?php $__env->startSection('content'); ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <div class="container myCustomFont">
        <h2 class="text-center"><?php echo e($page_title); ?></h2>
        
        <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="table-responsive">
            <table class="display table table-bordered table-striped" id="example">
                <thead>
                <tr>
                    
                    <th class="success">MEMBERS</th>
                    <th class="success">COMPLAINT TITLE</th>
                    <th class="success">SUBMITTED ON</th>
                    <th class="success">STATUS</th>
                    
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $committee): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                    <?php $__currentLoopData = $committee->complaints; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $complaint): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                        <td>
                            <?php $__currentLoopData = $committee->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $members): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                <strong class="myCustomFont"><span>  <?php echo e($members->user->full_name); ?></span> </strong>
                                <br>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                        </td>
                        <td>
                            <a href="<?php echo e(route('showSingleComplaint',$complaint->id)); ?>" id="link">
                                <div style="height:100%;width:100%">
                                    <?php echo e($complaint->title); ?>

                                </div>
                            </a>
                        </td>
                        <td><strong>  <?php echo e($complaint->created_at->format('jS F Y, h:i A')); ?>     </strong></td>
                        <td><strong>   <?php echo e($complaint->status); ?>          </strong></td>
                        </tr>

                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>




    <!-- copy start-->
    <?php echo e(Html::style('assets/data-tables/DT_bootstrap.css')); ?>

    <?php echo e(Html::script('js/jquery.js')); ?>

    <?php echo e(Html::script('assets/data-tables/jquery.dataTables.js')); ?>

    <?php echo e(Html::script('assets/data-tables/DT_bootstrap.js')); ?>


    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>