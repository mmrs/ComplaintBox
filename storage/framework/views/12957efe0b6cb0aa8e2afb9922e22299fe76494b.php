<?php $__env->startSection('content'); ?>

    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="container myCustomFont">
        <h2 class="text-center">All Committees</h2>
        
        
        
        
        

        <div class="table-responsive">

        <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <table class="display table table-bordered table-striped text-center" id="example">
            <thead>
            <tr>
                <th class="success text-center">MEMBERS</th>
                <th class="success text-center">TITLE</th>
                <th class="success text-center">CREATED ON</th>
                <th class="success text-center">STATUS</th>
                <?php if(\Illuminate\Support\Facades\Auth::user()->role=="HEAD"): ?>
                    <th class="success text-center"> ACTION</th> <?php endif; ?>
            </tr>
            </thead>

            <tbody>

            <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $committee): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                <tr>
                    
                    
                    <td>
                        <a href="<?php echo e(route('showCommittee',$committee['id'])); ?>">
                            <div style="height:100%;width:100%">
                                <?php $__currentLoopData = $committee['members']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                    <span> <?php echo $member; ?> </span>
                                    <br>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                            </div>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo e(route('showCommittee',$committee['id'])); ?>">
                            <div style="height:100%;width:100%">
                                <?php echo e($committee['title']); ?>

                            </div>
                        </a>
                    </td>
                    <td><strong>  <?php echo e($committee['created_at']->format('jS F, Y')); ?>  </strong></td>
                    <td><strong>   <?php echo e($committee['status']); ?>          </strong></td>
                    <?php if(\Illuminate\Support\Facades\Auth::user()->role=="HEAD"): ?>
                        <td>
                            <?php if($committee['status'] == "OPEN"): ?>
                                <a class="btn btn-warning confirm-alert"
                                   href="<?php echo route("closeCommittee", $committee['id']); ?>"> <i class="fa fa-lock"></i>
                                    CLOSE</a>
                            <?php else: ?>
                                <a class="btn btn-success confirm-alert"
                                   href="<?php echo route("openCommittee", $committee['id']); ?>"> <i class="fa fa-unlock"></i>
                                    OPEN</a>
                            <?php endif; ?>
                            <a class="btn btn-danger confirm-alert"
                               href="<?php echo route("deleteCommittee", $committee['id']); ?>"> <i class="fa fa-remove"></i>
                                DELETE</a>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>
        </table>
        </div>
    </div>
    

    <?php echo Html::script('/js/bootbox.min.js'); ?>


    <script>
        $(document).on("click", ".confirm-alert", function (e) {
            e.preventDefault();
            var eventObject = this;
            bootbox.confirm("Are you sure to Perform this Operation?", function (result) {
                if (result) {
                    console.log("User confirmed dialog");
                    window.location = eventObject.href;

                } else {
                    console.log("User declined dialog");
                }
            });
        });
    </script>

    <!-- copy start-->
    <?php echo e(Html::style('assets/data-tables/DT_bootstrap.css')); ?>

    <?php echo e(Html::script('js/jquery.js')); ?>

    <?php echo e(Html::script('assets/data-tables/jquery.dataTables.js')); ?>

    <?php echo e(Html::script('assets/data-tables/DT_bootstrap.js')); ?>


    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>