<?php $__env->startSection('content'); ?>

    <h1>
        <div class="well text-center myCustomFont" style="padding: 23px 0px;"><strong>
                Welcome to Complaint Box<sup>Ⓒ</sup> </strong>
        </div>
    </h1>
    <h2 class="text-center myCustomFont" style="margin-top: 30px; margin-bottom: 15px;">
        Submit Your complaint here anything about your Department.
        <div>Don't be afraid. You can keep your complaint secret!!</div>
    </h2>
    <br>
    <div class="container col-lg-6 col-lg-offset-3 myCustomFont">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-commenting fa-5x"></i>
                            </div>
                            <a href="<?php echo route('complaintIndex'); ?>">
                                <div class="col-xs-9 text-center">
                                    <h1 class="huge"><?php echo e(\App\Complaint::count()); ?></h1>
                                    <p>Total Complaints</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-bar-chart fa-5x"></i>
                            </div>
                            <a href="<?php echo route('closedComplaint'); ?>">
                                <div class="col-xs-9 text-center">
                                    <h1 class="huge"><?php echo e(\App\Complaint::where('status','CLOSED')->count()); ?></h1>
                                    <p>Solved Complaints</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-user-secret fa-5x"></i>
                            </div>
                            <a href="<?php echo route('complaintIndex'); ?>">
                                <div class="col-xs-9 text-center">
                                    <h1 class="huge"><?php echo e(\App\Complaint::where('privacy','PUBLIC')->count()); ?></h1>
                                    <p>Public Complaints</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3">
                                <i class="fa fa-user fa-5x"></i>
                            </div>
                            <a href="<?php echo route('committeeIndex'); ?>">
                                <div class="col-xs-9 text-center">
                                    <h1 class="huge"><?php echo e(\App\Committee::where('status','OPEN')->count()); ?></h1>
                                    <p>Active Committees</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>