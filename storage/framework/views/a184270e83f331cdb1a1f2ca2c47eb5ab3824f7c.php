<?php $__env->startSection('content'); ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <div class="container myCustomFont">
        <h2 class="text-center">My Complaints</h2>

        <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="table-responsive">
            <table class="display table table-bordered table-striped table-responsive" id="example">
                <thead>
                <tr>
                    <th class="success">DEPT.</th>
                    <th class="success">COMPLAINT TITLE</th>
                    <th class="success">SUBMITTED ON</th>
                    <th class="success">PRIVACY</th>
                    <th class="success">STATUS</th>
                    <th class="success">ACTION</th>
                </tr>
                </thead>
                <tbody>

                <?php $__currentLoopData = $complaints; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $complaint): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                    <tr>
                        <td><?php echo e($complaint->department_code); ?></td>
                        <td>
                            <a href="<?php echo e(route('showSingleComplaint',$complaint->id)); ?>" id="link">
                                <div style="height:100%;width:100%">
                                    <?php echo e($complaint->title); ?>

                                </div>
                            </a>
                        </td>
                        <td><strong>   <?php echo e($complaint->created_at->format('jS F Y, h:i A')); ?>   </strong></td>
                        <td><strong>   <?php echo e($complaint->privacy); ?>          </strong></td>
                        <td><strong>   <?php echo e($complaint->status); ?>          </strong></td>
                        <td>
                            <?php if($complaint->status == "PENDING"): ?>
                                <a class="btn btn-danger btn-block"
                                   href="<?php echo route("cancelComplaint", $complaint->id); ?>"> <i class="fa fa-remove"></i> CANCEL</a>
                            <?php else: ?>
                                <strong><h5 class="text-center"> NONE</h5></strong>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- copy start-->
    <?php echo e(Html::style('assets/data-tables/DT_bootstrap.css')); ?>

    <?php echo e(Html::script('js/jquery.js')); ?>

    <?php echo e(Html::script('assets/data-tables/jquery.dataTables.js')); ?>

    <?php echo e(Html::script('assets/data-tables/DT_bootstrap.js')); ?>


    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>