<?php $__env->startSection('content'); ?>
    <div class="container myCustomFont">
        <div class="row">
            <br><br>
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <br><br>
                    
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="<?php echo route('postLogin'); ?> ">
                            <?php echo e(csrf_field()); ?>


                            <div class="text-center" style="padding: 30px">
                                <img src="<?php echo asset('/images/sust_logo.png'); ?>" alt="Logo" height="12%" width="12%">
                            </div>
                            <div class="form-group<?php echo e($errors->has('login_name') ? ' has-error' : ''); ?>">
                                <label for="login_name" class="col-md-4 control-label">LoginName</label>
                                <div class="col-md-6">
                                    <input id="login_name" type="text" class="form-control" name="login_name" value="<?php echo e(old('login_name')); ?>" required autofocus>
                                    <?php if($errors->has('login_name')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('login_name')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    <?php if($errors->has('password')): ?>
                                        <span class="help-block">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="">
                                        <label>
                                            <input id='remember' type="checkbox" name="remember" disabled> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Login
                                    </button>

                                    <a class="btn btn-link" href="<?php echo route('temrsAndConditions'); ?>">
                                        Terms & Conditions
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>