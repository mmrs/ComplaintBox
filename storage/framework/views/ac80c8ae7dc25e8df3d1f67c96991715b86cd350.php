<?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>










<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="container-fluid">
            <br><br>
            <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="panel-group">
                <div class="row">
                    <?php if($committee->status == "OPEN"): ?>
                        <div class="panel panel-success col-lg-8 col-lg-offset-2" style="padding: 0">
                            <?php else: ?>
                                <div class="panel panel-danger col-lg-8 col-lg-offset-2" style="padding: 0">
                                    <?php endif; ?>
                                    <div class="panel-heading text-center">
                                        <h1 class="text-center myCustomFont"><?php echo e($committee->title); ?></h1>
                                        <h4 class="myCustomFont">
                                            <label for="status"> Department : </label> <?php echo e($committee->department_code); ?>

                                            <label for="status"> Status : </label> <?php echo e($committee->status); ?><br>
                                            <label for="status"><i class="fa fa-clock-o"></i>
                                            </label> <?php echo e($committee->created_at->format('jS F Y, h:i A')); ?>

                                        </h4>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="nav">
                                            <h2 class="myCustomFont">Members:</h2>
                                            <?php $__currentLoopData = $committee->members; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $member): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                <li class="well"><label for="description"> User Name
                                                        : </label> <?php echo e($member->user->login_name); ?>

                                                    <br>
                                                    <label for="description"> Full Name
                                                        : </label> <?php echo e($member->user->full_name); ?> <br>
                                                    <label for="description"> Designation
                                                        : </label> <?php echo e($member->user->designation); ?></li>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                        </ul>
                                    </div>
                                </div>
                        </div>
                        <br>
                        <button class="btn btn-info col-lg-1 col-lg-offset-5"
                                onclick="goBack()"><i class="fa fa-backward"></i> Go Back
                        </button>
                </div>
            </div>
        </div>
        <br><br>
    </div>
  
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>