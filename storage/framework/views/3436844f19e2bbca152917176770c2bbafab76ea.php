<!DOCTYPE html>
<html lang="en">
<head>
    <title> Complaint Box</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php echo Html::style('css/bootstrap.min.css'); ?>

    <?php echo Html::style('fonts/font-awesome/css/font-awesome.css'); ?>

    <?php echo Html::style('css/checkbox-radio.css'); ?>

    <?php echo Html::style('css/my-fonts.css'); ?>

    <?php echo Html::style('css/my-style.css'); ?>


    <?php echo Html::script('js/jquery.min.js'); ?>

    <?php echo Html::script('js/bootstrap.min.js'); ?>

    <?php echo Html::script('js/myFunction.js'); ?>


</head>

<body style="">
<?php echo $__env->make('layouts.navigation_bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class= "page-content" style="padding-top: 60px; padding-bottom: 20px;">
<?php echo $__env->yieldContent('content'); ?>
</div>
<?php echo $__env->make('committee.modal_confirm_create_committee', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>

</html>
