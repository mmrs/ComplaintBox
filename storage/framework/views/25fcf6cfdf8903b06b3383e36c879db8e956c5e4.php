<style>
    .my-Font{
        font-family: myMaianFont;
        color: #c10c05
    }


</style>

<?php $__env->startSection('content'); ?>

        <div class="container">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                <?php echo e(Form::open(array('route' =>  'postSubmitReport',
                                                'method' => 'post',
                                                'class' => 'form-submit-report'
                                                ))); ?>

                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <h3 class="form-signin-heading myCustomFont text-center">Submit Complaint Report</h3>
                <hr>
                <h2 class="my-Font text-center">  Complaint title: "<?php echo e($complaint->title); ?> "</h2>
                <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="login-wrap">

                    
                    <?php echo e(Form::hidden('complaintId',$complaintId, null, array('class' => 'form-control'))); ?>


                    
                    <?php echo e(Form::hidden('userId',$userId, null, array('class' => 'form-control'))); ?>

                    <?php echo e(Form::label('', '', array('' => ''))); ?>

                    <?php echo e(Form::textarea('reportBody', '', array('class' => 'form-control', 'autofocus','required'))); ?>


                    <br>
                    <div class="text-center col-lg-6 col-lg-offset-3">
                        <a class="btn btn-warning" href="" onclick="goBack()"> <i class="fa fa-backward"></i> Back  </a>
                        <?php echo e(Form::submit('Submit', array('class' => 'btn btn-login btn-primary'))); ?>

                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>

        </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>