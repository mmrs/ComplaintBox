<!-- Modal -->
<div class="modal fade" id="assignCommitteeModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content myCustomFont">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title myCustomFont">Select Committee</h4>
            </div>
            <div class="modal-body">
                <ul class="nav">
                    <?php echo e(Form::open(array('route' =>  'postAssignCommittee',
                               'method' => 'post',
                               'class' => 'form-signin'
                               ))); ?>

                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                    <div class="login-wrap">
                        
                        <?php echo e(Form::hidden('complaint_id',$complaint->id, null, array('class' => 'form-control'))); ?>


                        <?php echo e(Form::label('committee_title', 'Committee Title', array('' => ''))); ?>

                        <?php echo e(Form::select('committee_id', $committees, null, array('class' => 'form-control'))); ?>


                        <br>
                        <?php echo e(Form::submit('Assign', array('class' => 'btn btn-login btn-info col-lg-2 col-lg-offset-5'))); ?>

                    </div>
                    <?php echo e(Form::close()); ?>

                </ul>

            </div>
            
                
                
            
        </div>
    </div>
</div>
