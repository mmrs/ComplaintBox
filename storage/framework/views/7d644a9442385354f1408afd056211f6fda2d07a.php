<?php $__env->startSection('content'); ?>


    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="container myCustomFont">
        <h2 class="text-center">My Committees</h2>
        
        <div class="table-responsive">
        <table class="display table table-bordered table-striped" id="example">
            <thead>
            <tr>
                
                <th class="success">TITLE</th>
                <th class="success">CREATED ON</th>
                <th class="success">STATUS</th>
                
            </tr>
            </thead>

            <tbody>

            <?php $__currentLoopData = $committees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index=>$committee): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                <tr id='tr' onclick="myFunction(this)">
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    <td>
                        <a href="<?php echo e(route('showCommittee',$committee->id)); ?>">
                            <div style="height:100%;width:100%">
                                <?php echo e($committee->title); ?>

                            </div>
                        </a>
                    </td>
                    <td><strong>  <?php echo e(date('F d, Y', strtotime($committee->created_at))); ?>    </strong></td>
                    <td><strong>   <?php echo e($committee->status); ?>          </strong></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
            </tbody>
        </table>
        </div>
    </div>




    <!-- copy start-->
    <?php echo e(Html::style('assets/data-tables/DT_bootstrap.css')); ?>

    <?php echo e(Html::script('js/jquery.js')); ?>

    <?php echo e(Html::script('assets/data-tables/jquery.dataTables.js')); ?>

    <?php echo e(Html::script('assets/data-tables/DT_bootstrap.js')); ?>


    <script type="text/javascript" charset="utf-8">
        $(document).ready(function () {

            $('#example').dataTable({
                stateSave: true
            });

        });
    </script>
    <!-- copy end-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>