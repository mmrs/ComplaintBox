<?php $__env->startSection('content'); ?>
    <div class="register-body">
        <div class="container">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                <?php echo e(Form::open(array('route' =>  'postAssignMember',
                                                'method' => 'post',
                                                'class' => 'form-signin'
                                                ))); ?>

                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <h2 class="form-signin-heading myCustomFont text-center">Assign Member to Committee</h2>
                <hr>
                <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="login-wrap">

                    <br>
                    <?php echo e(Form::label('committee_title', 'Committee Title*', array('' => ''))); ?>

                    <?php echo e(Form::select('committee_id', $committees, null, array('class' => 'form-control myCustomFont'))); ?>


                    
                    
                    <h3 class="myCustomFont">Select Members For This Committee</h3>
                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

                        <div class="checkbox checkbox-success myCustomFont">
                            <?php echo e(Form::checkbox('user_id[]', $user->id, null, ['class' => 'field'])); ?>

                            <?php echo e(Form::label('user_name', $user->full_name, array('class' => 'myCustomFont'))); ?><br>
                        </div>


                    <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

                    <br><br>
                    <div class="text-center col-lg-6 col-lg-offset-3 myCustomFont">
                        <a class="btn btn-warning" href="" onclick="goBack()"> <i class="fa fa-backward"></i> Back</a>
                    <?php echo e(Form::submit('Assign Member', array('class' => 'btn btn-login btn-primary'))); ?>


                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>