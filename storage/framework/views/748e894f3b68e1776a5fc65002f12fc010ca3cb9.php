<?php $__env->startSection('content'); ?>

    <div class="container" style="width: 100%">

        <?php if($complaint->status == "CLOSED"): ?>
            <div class="panel panel-success col-lg-8 col-lg-offset-2" style="padding: 0">
                <?php elseif($complaint->status == "REJECTED"): ?>
                    <div class="panel panel-danger col-lg-8 col-lg-offset-2" style="padding: 0">
                        <?php elseif($complaint->status == "PENDING"): ?>
                            <div class="panel panel-warning col-lg-8 col-lg-offset-2" style="padding: 0">
                                <?php elseif($complaint->status == "IN-PROGRESS"): ?>
                                    <div class="panel panel-info col-lg-8 col-lg-offset-2" style="padding: 0">
                                        <?php else: ?>
                                            <div class="panel panel-default col-lg-8 col-lg-offset-2"
                                                 style="padding: 0">
                                                <?php endif; ?>
                                                <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                <div class="panel-heading text-center">
                                                    <div class="myCustomFont ">
                                                        <h2 class="text-center myCustomFont"><?php echo e($complaint->title); ?></h2>
                                                        <h4 class="text-center myCustomFont"><label for="name"> <i
                                                                        class="fa fa-user"></i>
                                                            </label>
                                                            <a href="" data-toggle="tooltip" data-placement="bottom"
                                                               title="<?php echo e("Dept: "  . $user->department_code); ?> "> <?php echo e($user->full_name); ?> </a>
                                                            
                                                            
                                                            
                                                            <label for="submitted"> <i class="fa fa-clock-o"></i>
                                                            </label> <?php echo e($complaint->created_at->format('jS F Y, h:i A')); ?>

                                                            <br>
                                                        </h4>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <ul class="nav">
                                                        <li class="well">
                                                            <label for="status"> Category
                                                                : </label> <?php echo e($category->display_name); ?>

                                                            <br>
                                                            <label for="privacy"> Privacy
                                                                : </label> <?php echo e($complaint->privacy); ?>

                                                            <label for="status"> Status : </label>
                                                            <strong> <?php echo e($complaint->status); ?> </strong> <br>
                                                            <label for="description"> Description
                                                                : </label> <?php echo e($complaint->description); ?></li>


                                                        <?php if((\Illuminate\Support\Facades\Auth::user()->role == "HEAD"||
                                                        (\Illuminate\Support\Facades\Auth::user()->role == "COMMITTEE" && $complaint->privacy == "PUBLIC")
                                                        || $isSubmitReport == "true") &&
                                                          ($complaint->status =="IN-PROGRESS" || $complaint->status =="CLOSED")): ?>
                                                            <ul class="well">
                                                                <?php $__currentLoopData = $assigned; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                                    <label for="title">Committe Name
                                                                        :</label> <?php echo e($index['title']); ?>

                                                                    <label for="members"> <i class="fa fa-users"></i>
                                                                    </label> <?php echo e($index['names']); ?>

                                                                    <br>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                        <?php if((\Illuminate\Support\Facades\Auth::user()->role == "HEAD" || $isSubmitReport == "true") &&
                                                          ($complaint->status =="IN-PROGRESS" || $complaint->status =="CLOSED")): ?>
                                                            <ul class="well">
                                                                <label for="comments"> Comments :</label>
                                                                <br>
                                                                <?php $__currentLoopData = $complaint->reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $report): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
                                                                    <span> " <?php echo e($report->comment); ?> "</span>
                                                                    <br>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
                                                            </ul>
                                                        <?php endif; ?>

                                                        <div class="btn-group text-center myCustomFont">

                                                            <?php if(\App\UserPenalize::select('penalize_date')
                                                            ->where('user_id', '=', $user->id)->max('penalize_date') > \Carbon\Carbon::now()): ?>

                                                            <?php elseif(($isSubmitReport || \Illuminate\Support\Facades\Auth::user()->role == "HEAD")
                                                             && $complaint->status!="CANCELLED" && $complaint->status!="CLOSED"): ?>
                                                                <a class="btn btn-warning confirm-alert"
                                                                   style="margin-right: 15px; margin-top: 5px;"
                                                                   href="<?php echo route("penalizeUser", ["user"=>$user->id,"complaint"=>$complaint->id]); ?>">
                                                                    <i class="fa fa-lock"></i> Penalize User</a>
                                                            <?php endif; ?>

                                                            <?php if(\Illuminate\Support\Facades\Auth::user()->role == "HEAD" && $complaint->status == "PENDING"): ?>
                                                                <a href="<?php echo route('rejectComplaint', ['id' => $complaint->id]); ?> "
                                                                   style="margin-right: 15px;margin-top: 5px;"
                                                                   class="btn btn-danger">
                                                                    <i class="fa fa-close"></i> Reject Complaint</a>
                                                            <?php endif; ?>

                                                            <?php if(\Illuminate\Support\Facades\Auth::user()->role == "HEAD" && $complaint->status !="REJECTED"
                                                                && $complaint->status !="CLOSED" && $complaint->status!="CANCELLED"  ): ?>
                                                                <?php echo $__env->make('committee.modal_assign_committee', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                                                <a class="btn btn-primary" style="margin-right: 15px;margin-top: 5px;"
                                                                   data-toggle="modal"
                                                                   data-target="<?php echo e('#assignCommitteeModal'); ?>" href="">
                                                                    <i class="fa fa-dedent"></i>
                                                                    Assign Committee
                                                                </a>
                                                            <?php endif; ?>

                                                            <?php if($isSubmitReport && $complaint->status !="REJECTED"
                                                                 && $complaint->status!="CANCELLED"): ?>
                                                                <a class="btn btn-success" style="margin-right: 15px;margin-top: 5px;"
                                                                   href="
                                                            <?php echo e(route('getSubmitReport',['complaint'=>$complaint->id])); ?>"
                                                                   role="button"> <i class="fa fa-comment"></i> Submit
                                                                    Report </a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </ul>
                                                </div>
                                            </div>
                                            <br>
                                            <button class="btn btn-info col-lg-1 col-lg-offset-5"
                                                    onclick="goBack()">
                                                <i class="fa fa-backward"></i> Go Back
                                            </button>
                                    </div>
                            </div>
                    </div>
            </div>
    </div>


    <?php echo e(Html::style('/fonts/font-awesome/css/font-awesome.css')); ?>

    
    <?php echo e(Html::script('/js/bootbox.min.js')); ?>


    <script>
        $(document).on("click", ".confirm-alert", function (e) {
            e.preventDefault();
            var eventObject = this;
            bootbox.confirm("This user will not be able to submit another complaint in next 15 days. Are you sure to Perform this Operation?", function (result) {
                if (result) {
                    console.log("User confirmed dialog");
                    window.location = eventObject.href;

                } else {
                    console.log("User declined dialog");
                }
            });
        });
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>