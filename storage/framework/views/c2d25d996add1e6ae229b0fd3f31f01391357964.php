<?php $__env->startSection('content'); ?>
    <div class="register-body">
        <div class="container submitForm">
            <div class="col-lg-6 col-sm-6 address col-lg-offset-3">
                <?php echo e(Form::open(array('route' =>  'postComplaintSubmit',
                                                'method' => 'post',
                                                'class' => 'form-submit-complaint',
                                                'name' => 'myForm'
                                                ))); ?>

                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                <h1 class="form-signin-heading myCustomFont text-center">Submit New Complaint</h1>
                <hr>
                <?php echo $__env->make('includes.alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <div class="login-wrap">
                    <?php echo e(Form::label('complaint_title', 'Complaint Title*', array('' => ''))); ?>

                    <?php echo e(Form::text('title', '', array('class' => 'form-control', 'autofocus','required','placeholder'=>'Complaint Title','name'=>'title'))); ?>


                    <?php echo e(Form::label('concerned_department', 'Concerned Department*', array('' => ''))); ?>

                    <?php echo e(Form::select('department_code', $department, null, array('class' => 'form-control'))); ?>


                    <?php echo e(Form::label('category', 'Complaint Category*', array('' => ''))); ?>

                    <?php echo e(Form::select('category', $category, null, array('class' => 'form-control'))); ?>

                    <?php echo e(Form::label('privacy', 'Privacy*', array('' => ''))); ?>


                    


                    <div class="radio radio-danger">
                    <?php echo e(Form::radio('privacy','PRIVATE')); ?>

                    <?php echo e(Form::label('privacy', 'PRIVATE', array('class' => 'myCustomFont'))); ?>

                        <br>
                    <?php echo e(Form::radio('privacy', 'PUBLIC',true)); ?>

                    <?php echo e(Form::label('privacy', 'PUBLIC', array('class' => 'myCustomFont'))); ?>

                    </div>

                    <?php echo e(Form::label('description', 'Complaint Details*', array('' => ''))); ?>

                    <?php echo e(Form::textarea('description', '', array('class' => 'form-control', 'autofocus','required','placeholder'=>'Complaint Details....','name'=>'description'))); ?>


                    *all fields are mandatory
                    <br><br><br>
                    <div class="col-lg-4 col-lg-offset-4">
                        <?php echo e(Form::submit('Submit', array('class' => 'btn btn-block btn-login btn-primary confirm-alert'))); ?>

                    </div>
                </div>
                <?php echo e(Form::close()); ?>

            </div>

        </div>
        </br>
    </div>

    <?php echo Html::script('/js/bootbox.min.js'); ?>


    <script>
        $(document).on("click", ".confirm-alert", function(e) {
            e.preventDefault();
            if(!validateForm())
                return;
            bootbox.confirm("Are you sure to submit this Complaint?", function(result) {
                if (result) {
                    console.log("User confirmed dialog");
                    var x = document.getElementsByClassName("form-submit-complaint");
                    x[0].submit(); // Form submission
                } else {
                    console.log("User declined dialog");
                }
            });
        });

        function validateForm() {
            var x = document.forms["myForm"]["title"].value;
            var y = document.forms["myForm"]["description"].value;
            if (x == "" || y=="") {
                bootbox.alert("Fill all the fields correctly. All fields are mandatory.");
                return false;
            }
            return true;
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>