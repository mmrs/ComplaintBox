<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('reports', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('complaint_id')->unsigned();
//            $table->integer('committee_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('comment');
            $table->timestamps();

//            $table->unique(['complaint_id','committee_id']);
//            $table->unique(['complaint_id','user_id']);

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('complaint_id')->references('id')->on('complaints')
                ->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('committee_id')->references('id')->on('committees')
//               ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
