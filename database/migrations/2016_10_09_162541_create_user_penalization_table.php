<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPenalizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_penalizes', function (Blueprint $table) {

           $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->integer('complaint_id')->unsigned();
            $table->string('department_code');
            $table->timestamp('penalize_date');

            $table->unique(['user_id','complaint_id']);

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('complaint_id')->references('id')->on('complaints')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
