<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignedCommitteeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('assigned_committee', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('complaint_id')->unsigned();
            $table->integer('committee_id')->unsigned();
            $table->string('department_code');
            $table->timestamps();

            $table->unique(['complaint_id','committee_id']);

            $table->foreign('complaint_id')->references('id')->on('complaints')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('committee_id')->references('id')->on('committees')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
