<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(
            [
                'login_name' => 'mmrs',
                'full_name' => 'Md. Moshiur Rahman',
                'department_code' => 'CSE',
                'designation' => 'Student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012331049',
            ]);
        User::create(
            [
                'login_name' => 'olee',
                'full_name' => 'M. Tahmid Hossain',
                'department_code' => 'CSE',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012331017',
            ]);
        User::create(
            [
                'login_name' => 'shopon',
                'full_name' => 'Md. Habibur Rahman',
                'department_code' => 'CSE',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012331029',
            ]);
        User::create(
            [
                'login_name' => 'sharif',
                'full_name' => 'Sharif Siddique',
                'department_code' => 'CSE',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012331025',
            ]);

        User::create(
            [
                'login_name' => 'akash',
                'full_name' => 'Saumik Kumar Saha',
                'department_code' => 'EEE',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012345012',
            ]);

        User::create(
            [
                'login_name' => 'abdullah',
                'full_name' => 'Md. Abdullah',
                'department_code' => 'IPE',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012325014',
            ]);
        User::create(
            [
                'login_name' => 'rumie',
                'full_name' => 'Rumie Miraj Ul Amin',
                'department_code' => 'PHY',
                'designation' => 'student',
                'password' => 'aaaaaaaa',
                'role' => 'USER',
                'registration_no' => '2012145019',
            ]);

        User::create(
            [
                'login_name' => 'headcse',
                'full_name' => 'Dr Mohammad Reza Selim',
                'department_code' => 'CSE',
                'designation' => 'Professor',
                'password' => 'aaaaaaaa',
                'role' => 'HEAD',
            ]
        );
        User::create(
            [
                'login_name' => 'headeee',
                'full_name' => 'Dr Mohammad Shahidur Rahman',
                'department_code' => 'EEE',
                'designation' => 'Professor',
                'password' => 'aaaaaaaa',
                'role' => 'HEAD',
            ]
        );

        User::create(
            [
                'login_name' => 'headipe',
                'full_name' => 'Dr Mohammad Muhshin Aziz Khan',
                'department_code' => 'IPE',
                'designation' => 'Professor',
                'password' => 'aaaaaaaa',
                'role' => 'HEAD',
            ]
        );
        User::create(
            [
                'login_name' => 'headphy',
                'full_name' => 'Dr. Mr. Head of PHY',
                'department_code' => 'PHY',
                'designation' => 'Professor',
                'password' => 'aaaaaaaa',
                'role' => 'HEAD',
            ]
        );
        User::create(
            [
                'login_name' => 'teachercse3',
                'full_name' => 'Md. Saiful Islam',
                'department_code' => 'CSE',
                'designation' => 'Assistant Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachercse2',
                'full_name' => 'Sabir Ismail',
                'department_code' => 'CSE',
                'designation' => 'Assistant Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachercse1',
                'full_name' => 'Md Masum',
                'department_code' => 'CSE',
                'designation' => 'Associate Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachercse4',
                'full_name' => 'Biswapriyo Chakrabarty',
                'department_code' => 'CSE',
                'designation' => 'Lecturer',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachereee1',
                'full_name' => 'Tuhin Dey',
                'department_code' => 'EEE',
                'designation' => 'Assistant Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachereee2',
                'full_name' => 'Biswajit Paul',
                'department_code' => 'EEE',
                'designation' => 'Assistant Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teachereee3',
                'full_name' => 'Mohammad Kamruzzaman Khan Prince',
                'department_code' => 'EEE',
                'designation' => 'Lecturer',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teacherphy1',
                'full_name' => 'Dr. Md. Shah Alam',
                'department_code' => 'PHY',
                'designation' => 'Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );
        User::create(
            [
                'login_name' => 'teacheripe1',
                'full_name' => 'Engr A B M Abdul Malek',
                'department_code' => 'IPE',
                'designation' => 'Associate Professor',
                'password' => 'aaaaaaaa',
                'role' => 'COMMITTEE',
            ]
        );

    }

}
