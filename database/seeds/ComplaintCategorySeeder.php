 <?php

use Illuminate\Database\Seeder;
use App\ComplaintCategory;

class ComplaintCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        ComplaintCategory::create(
        [
            'category_name' => 'category1',
            'display_name' => 'Class Related Issues'

        ]
        );
        ComplaintCategory::create(
            [
                'category_name' => 'category2',
                'display_name' => 'Lab Related Issues'

            ]
        );
        ComplaintCategory::create(
            [
                'category_name' => 'category3',
                'display_name' => 'Office Employee Related Issues'

            ]
        );
        ComplaintCategory::create(
            [
                'category_name' => 'category4',
                'display_name' => 'Teacher Related Issues'

            ]
        );
        ComplaintCategory::create(
            [
                'category_name' => 'category5',
                'display_name' => 'Other Issues'

            ]
        );

    }
}
