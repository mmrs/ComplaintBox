<?php

use Illuminate\Database\Seeder;
use App\Department;
class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::create(
            [
                'department_code' => 'CSE',
                'display_name' => 'Computer Science & Engineering',
            ]);
        Department::create(
        [
            'department_code' => 'EEE',
            'display_name' => 'Electrical & Electronic Engineering',
        ]);

        Department::create(
            [
                'department_code' => 'IPE',
                'display_name' => 'Industrial & Production Engineering',
            ]);
        Department::create(
            [
                'department_code' => 'PHY',
                'display_name' => 'Physics',
            ]);

    }
}
