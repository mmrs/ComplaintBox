<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\Http\Requests;
use App\Committee;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Complaint;
use App\ComplaintCategory;
use Illuminate\Support\Facades\Input;

Route::get('/', function () {

    return \Illuminate\Support\Facades\Redirect::to('/home');
});




Route::get('/termsAndConditions',function (){

    return view('termsandcondition');

})->name('temrsAndConditions');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->middleware('myAuthFailed')->name('dashboard');
//user Controller

Route::get('user/login','UserController@getLogin')->middleware('myAuthPassed')->name('getLogin');
Route::post('user/login','UserController@postLogin')->middleware('myAuthPassed')->name('postLogin');
Route::get('user/logout','UserController@logout')->middleware('myAuthFailed')->name('getLogout');;
Route::get('user/penalize/','UserController@penalizeUser')->middleware(['myAuthFailed','authCommittee'])->name('penalizeUser');
Route::get('user/{id}','UserController@showUser')->middleware('myAuthFailed')->name('showUser');


// Complaint Controller
Route::get('complaint/index','ComplaintController@index')->middleware('myAuthFailed')->name('complaintIndex');
Route::get('complaint/my','ComplaintController@myComplaints')->middleware('myAuthFailed')->name('complaintMy');
Route::get('complaint/submit','ComplaintController@getSubmitNewComplaint')->middleware('myAuthFailed')->name('getComplaintSubmit');
Route::post('complaint/submit','ComplaintController@postSubmitNewComplaint')->middleware('myAuthFailed')->name('postComplaintSubmit');
Route::get('complaint/pending','ComplaintController@showPendingComplaint')->middleware('myAuthFailed','authAdmin')->name('pendingComplaint');
Route::get('complaint/closed','ComplaintController@getClosedComplaint')->middleware('myAuthFailed')->name('closedComplaint');
Route::get('complaint/view/{id}','ComplaintController@showSingleComplaint')->middleware('myAuthFailed')->name('showSingleComplaint');
Route::post('complaint/assign/committee','ComplaintController@postAssignCommittee')->middleware(['myAuthFailed','authAdmin'])->name('postAssignCommittee');
Route::get('complaint/reject/{id}','ComplaintController@rejectComplaint')->middleware(['myAuthFailed','authAdmin'])->name('rejectComplaint');
Route::get('complaint/cancel/{id}','ComplaintController@cancelComplaint')->middleware(['myAuthFailed'])->name('cancelComplaint');
Route::get('complaint/my-committee/','ComplaintController@myCommitteeComplaints')->middleware(['myAuthFailed','authCommittee'])->name('myCommitteeComplaint');
Route::get('complaint/report','ComplaintController@getSubmitReport')->middleware(['myAuthFailed','authCommittee'])->name('getSubmitReport');
Route::post('complaint/report','ComplaintController@postSubmitReport')->middleware(['myAuthFailed','authCommittee'])->name('postSubmitReport');
//Route::get('complaint/viewCom/{id}','ComplaintController@showSingleComplaint')->middleware(['myAuthFailed','authCommittee'])->name('showSingleComplaintCommittee');


// Committee Controller
Route::get('committee/index','CommitteeController@index')->middleware(['myAuthFailed','authCommittee'])->name('committeeIndex');
Route::get('committee/my','CommitteeController@my')->middleware(['myAuthFailed','authCommittee'])->name('committeeMy');
Route::get('committee/new','CommitteeController@postCreate')->middleware(['myAuthFailed','authAdmin'])->name('newCommittee');
Route::get('committee/assign/member','CommitteeController@getAssignMember')->middleware(['myAuthFailed','authAdmin'])->name('getAssignMember');
Route::post('committee/assign/member','CommitteeController@postAssignMember')->middleware(['myAuthFailed','authAdmin'])->name('postAssignMember');
Route::get('committee/view/{id}','CommitteeController@showCommittee')->middleware(['myAuthFailed','authCommittee'])->name('showCommittee');
Route::get('committee/delete/{id}','CommitteeController@deleteCommittee')->middleware(['myAuthFailed','authAdmin'])->name('deleteCommittee');
Route::get('committee/close/{id}','CommitteeController@closeCommittee')->middleware(['myAuthFailed','authAdmin'])->name('closeCommittee');
Route::get('committee/open/{id}','CommitteeController@openCommittee')->middleware(['myAuthFailed','authAdmin'])->name('openCommittee');
//test
Route::get('test',function (\Illuminate\Http\Request $request){

return Input::get('id').Input::get('user');



});
