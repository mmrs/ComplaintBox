<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MemberCommittee extends Model
{
    protected $table = "member_committee";
    protected $with = ['user'];
    protected $fillable= [
        'committee_id',
        'user_id',
        'department_code',
    ];

    public function committee(){

        return $this->belongsToMany('App\Committee');
    }
    public function user(){

        return $this->belongsTo('App\User');
    }


}
