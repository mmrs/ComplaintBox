<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Complaint extends Model
{
    //
    protected $table = "complaints";
    protected $fillable= [
        'user_id',
        'title',
        'department_code',
        'description',
        'category_id',
        'privacy',
        'status'

    ];

    public function reports(){
        return $this->hasMany('\App\Report');
    }

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function committees(){
        return $this->belongsToMany('\App\Committee','assigned_committee','complaint_id','committee_id');
    }

}
