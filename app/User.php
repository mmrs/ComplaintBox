<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Complaint;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login_name',
        'full_name',
        'email',
        'password',
        'designation',
        'registration_no','role'
    ];

    //could use guarded[] as blank to mean all fillable


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'remember_token','password'
    ];


    public function complaints(){

        return $this->hasMany('\App\Complaint');
    }

    public function penalizes(){
        return $this->hasMany('\App\UserPenalize');
    }

}
