<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
     protected $table = "reports";
    protected $fillable= [
        'complaint_id',
        'committee_id',
        'user_id',
        'comment'

    ];
}
