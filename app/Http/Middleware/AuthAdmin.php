<?php

namespace App\Http\Middleware;

use App\User;
use Closure;


use Illuminate\Support\Facades\Auth;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if($request->user()->role == 'HEAD'){

            return $next($request);
        }
//        else return response("Unauthorized!!",401);
        else return response(view('errors.access-denied'));

    }
}
