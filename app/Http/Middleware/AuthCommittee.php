<?php

namespace App\Http\Middleware;

use Closure;


use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\Response;
class AuthCommittee
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->role == 'USER'){
//            return response("Sorry You Are Not Authorized for this operation.",401);
            return response(view('errors.access-denied'));
        }
        return $next($request);
    }
}
