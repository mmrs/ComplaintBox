<?php

namespace App\Http\Controllers;

use App\MemberCommittee;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Committee;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class CommitteeController extends Controller
{
    //
    public function index(){
        $committees = Committee::where('department_code',Auth::user()->department_code)->orderBy("id","desc")
            ->with('members')
            ->get();
        $i = 0;
        $result = [];
 //       return $committees;
        foreach ($committees as $committee){
            $result[$i]['id'] = $committee->id;
            $result[$i]['title'] = $committee->title;
            $result[$i]['status'] = $committee->status;
            $result[$i]['created_at'] = $committee->created_at;
            $result[$i]['department_code'] = $committee->department_code;

            $result[$i]['members'] = [];

            foreach ($committee->members as $member){
                $result[$i]['members'][] = $member->user->full_name;
            }

            $i++;

        }
     //   return $result;
        return view('committee.view_committees_index')->with(['committees'=>$result]);

    }


    public function my(){

        $user = User::find(Auth::id());
        $committees = DB::select(
            'select  committees.id,title, status,committees.created_at from committees,member_committee
             where member_committee.user_id= ' . $user->id   .' AND committees.id= member_committee.committee_id order by 
             committees.id'
        );


        return view('committee.view_my_committees')->with(['committees'=>$committees]);

    }


    public function showCommittee($id){

        $committee = Committee::with('members')->where('id',$id)->get();
        $committee = $committee[0];

        return view('committee.view_committee_details')->with(['committee'=>$committee]);

    }

    public function getAssignMember(){

        $users = User::where('department_code',Auth::user()->department_code)
            ->where('role','!=','USER')->get();
//        $users = array_pluck($users,'full_name','id');

        $committees = Committee::where('department_code',Auth::user()->department_code)
                                                    ->orderBy('id','desc')->get();
        $committees = array_pluck($committees,'title','id');
//        return $users;

        return view('committee.form_assign_committee_member')->with(['users'=>$users,'committees' =>$committees]);
    }

    public function postAssignMember(Request $request){

         $user_ids = Input::get('user_id');
         if(sizeof($user_ids) == 0){

            $request->session()->flash('error', "No member was selected!");
            return redirect::back();
         }
        $committee_id = Input::get('committee_id');
//        echo $committee_id;
            foreach ($user_ids as $user_id){

//                echo 'user id '.$user_id;
                if(!DB::select('select * from member_committee where committee_id='. $committee_id.
                          ' and user_id ='.$user_id)){

//                    echo '-assigned\n';
                    MemberCommittee::create(
                        [
                            'committee_id' => $committee_id,
                            'user_id' => $user_id,
                            'department_code' => Auth::user()->department_code
                        ]
                    );
                }
//                else echo 'already assigned\n';

            }
            $message = "Members were successfully assigned!!";
            $request->session()->flash('success', $message);
            return redirect::to('/committee/index');
    }


    public function postCreate(){

        $rules =[
            'title' =>  'required|max:100',
        ];
        $data = Input::all();
        $validation = Validator::make($data,$rules);

        if($validation->fails()){

            return response()->json(array('msg'=> "Validation failed."), 400);
        }
        $title = Input::get('title');
//        $title = 'hey';
        \App\Committee::create(
            [
                'title' => $title,
                'department_code' => Auth::user()->department_code
            ]
        );
        return response()->json(array('msg'=> "Committee \"".$title."\" created Successfully."), 200);
    }

    public function deleteCommittee($id){

        $committee  = Committee::find($id);
        $committee->delete();
        return redirect::back();

    }

    public function closeCommittee($id){

        $committee  = Committee::find($id);
       // return $committee;
        $committee->status = "CLOSED";
        $committee->save();
        return redirect::back();

    }

    public function openCommittee($id){

        $committee  = Committee::find($id);
        // return $committee;
        $committee->status = "OPEN";
        $committee->save();
        return redirect::back();

    }

}
