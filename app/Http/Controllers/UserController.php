<?php

namespace App\Http\Controllers;

use App\Complaint;
use App\UserPenalize;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
class UserController extends Controller
{

    public function getLogin(){

            return View('user.login');
    }

    public  function postLogin(Request $request){

        $user = User::where('login_name', $request->get('login_name'))
            ->where('password', $request->get('password'))
            ->first();

        if($user){
            if($request->get('remember')) {
                Auth::login($user, true);

            } else {
                Auth::login($user);
            }
            return redirect('/dashboard');
        }
        else {
            return Redirect::back()->withInput()
                                   ->withErrors([
                                       'login_name'=>'Please re-check Log in ID.',
                                       'password' => 'Please re-check your password.'
                                   ]);
        }
    }

    public function logout(){

        Auth::logout();
        Auth::clearResolvedInstances();
        return redirect('/user/login');
    }



//    public function showUser($id){
//
//        return User::find($id);
//
//    }

    public function penalizeUser(){

        $complaint = Complaint::find(Input::get("complaint"));
        $userId = Input::get('user');
        $complaintId = $complaint->id;
        $res[] = Validations::isValidated($complaintId);

        if((Auth::user()->role=="HEAD" || $res[0]['isSubmit']) && $complaint->status!="CANCELLED" && $complaint->status!="CLOSED" && $complaint->user_id == $userId){

            UserPenalize::create([
                'user_id' =>  Input::get('user'),
                'complaint_id' => Input::get('complaint'),
                'department_code' => Auth::user()->department_code,
                'penalize_date' => Carbon::now()->addDays(15)
            ]);

            return Redirect::back();

        }
        else  return response(view('errors.access-denied'));

    }
}
