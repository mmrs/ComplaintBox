<?php

namespace App\Http\Controllers;



use App\AssignedCommittee;
use App\Committee;
use App\Http\Requests;
use App\Complaint;

//use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;


class Validations
{

    public static function  isValidated($complaintId){

        $complaint = Complaint::find($complaintId);

        if($complaint->department_code != Auth::user()->department_code)
        {
            return response(view('errors.access-denied'));
        }

        $isSubmit = false;
        $isAssigned = false;
        $complaint = Complaint::where('id',$complaintId)->with('reports')->first();


        $assigned = AssignedCommittee::where('complaint_id',$complaint->id)->get();
        $result = [];
        $i = 0;
        foreach($assigned as $assigned){

            $committeeMembers = Committee::where('id',$assigned->committee_id)
                ->with('members')
                ->get();

            foreach ($committeeMembers as $committeeMember){

                $isOpen = $committeeMember->status;
                $result[$i]['title']=$committeeMember->title;
                foreach ($committeeMember->members as $member){

                    if(Auth::user()->id == $member->user->id)
                        $isAssigned = true;

                    if($isOpen == "OPEN" && Auth::user()->id == $member->user->id){
                        $isSubmit = true;
                    }
                }
                $i++;
            }
        }

        $isMy = false;
        $authUser = Auth::user() ;
        if($authUser->id == $complaint->user_id) $isMy = true;

        return ['isMy' => $isMy, 'isAssigned'=>$isAssigned, 'isSubmit' => $isSubmit];
    }
}
