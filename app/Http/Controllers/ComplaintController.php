<?php

namespace App\Http\Controllers;

use App\AssignedCommittee;
use App\Committee;
use App\ComplaintCategory;
use App\Department;
use App\MemberCommittee;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Report;
use App\Http\Requests;
use App\Complaint;
use App\User;
use App\UserPenalize;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;




use Yajra\Datatables\Facades\Datatables;

class ComplaintController extends Controller
{
    // retuns all complaints to head according to department

    public function index(){
/*al complaints*/
        $user = Auth::user();
        if($user->role != 'HEAD')
        $complaints = Complaint::with('user')->orderBy('created_at','desc')
            ->where('department_code','=',$user->department_code)
            ->where('privacy','PUBLIC')
            ->get();
        else
            $complaints = Complaint::with('user')->orderBy('created_at','desc')
                ->where('department_code','=',$user->department_code)
                ->get();

//       return $complaints;
        return view('complaint.complaint_index')->with(['complaints'=>$complaints,'page_title'=>'All Complaints']);

    }


    public  function myComplaints(){

/*        $complaints = User::find(Auth::id())->complaints;
        $complaints = DB::select('SELECT users.id as user_id,login_name,complaints.id as complaint_id,status,complaints.department_code,title
                                  from users,complaints
                                  WHERE user_id = '.Auth::id() .' AND complaints.user_id = users.id ORDER BY complaints.updated_at DESC');*/

        $complaints = Complaint::with('user')->where('user_id',Auth::id())->orderBy('created_at','desc')->get();

        return view('complaint.my_complaint')->with(['complaints'=>$complaints]);
    }


public function getClosedComplaint(){

  $user = Auth::user();
  if($user->role != 'HEAD')
  $complaints = Complaint::with('user')->orderBy('created_at','desc')
      ->where('department_code','=',$user->department_code)
      ->where('privacy','PUBLIC')
      ->where('status','CLOSED')
      ->get();
  else
      $complaints = Complaint::with('user')->orderBy('created_at','desc')
          ->where('department_code','=',$user->department_code)
          ->where('status','CLOSED')
          ->get();

//       return $complaints;
  return view('complaint.complaint_index')->with(['complaints'=>$complaints,'page_title'=>'All Complaints']);


}
    public function getSubmitNewComplaint(){


        $category = ComplaintCategory::pluck('display_name','id')->all();
        $department = Department::pluck('display_name','department_code')->all();

        return view('complaint.submit_complaint')->with(['category'=>$category,'department' =>$department]);


    }

    public function postSubmitNewComplaint(Request $request){

/*        return $request->all();

        return $request->get('title');
        return Auth::user()->id;*/

        $time = UserPenalize::select('penalize_date')
                ->where('user_id', '=', Auth::user()->id)->max('penalize_date') > \Carbon\Carbon::now();
        if($time){

            $timeStamp = UserPenalize::select('penalize_date')
                    ->where('user_id', '=', Auth::user()->id)->max('penalize_date');
           $date = date( "M-d Y", strtotime($timeStamp));
            $message = 'Dear ' . Auth::user()->full_name . '! You have been penalized. You can not submit complaint before '.$date .'.';
            $request->session()->flash('error', $message);
            return Redirect::back();
        }




        $rules =[
            'title'              =>  'required|max:150',
            'description'        =>  'required'
        ];

        $data = Input::all();

        $validation = Validator::make($data,$rules);

        if($validation->fails()){
            return Redirect::back()->withErrors($validation)->withInput(Input::all());
        }

        Complaint::create([

            'user_id' => Auth::user()->id,
            'title' => $request->get('title'),
            'department_code' => $request->get('department_code'),
            'description' => $request->get('description'),
            'privacy' => $request->get('privacy'),
            'category_id' => $request->get('category')
        ]);

        $message = 'Dear ' . Auth::user()->full_name . '! We received your complaint successfully. It is currently under review.';
        $request->session()->flash('success', $message);
        return redirect::to('complaint/my');

    }


    public function  showSingleComplaint($id){

        $isSubmit = false;
        $isAssigned = false;
        $complaint = Complaint::where('id',$id)->with('reports')->first();
//        return $complaint;

        $user = User::find($complaint->user_id);
        $category = ComplaintCategory::find($complaint->category_id);
        $committees = Committee::where('department_code',Auth::user()->department_code)
            ->where('status','OPEN')
            ->orderBy('id','desc')->get();
        $committees = array_pluck($committees,'title','id');
//        return $complaint;

        $assigneds = AssignedCommittee::where('complaint_id',$complaint->id)->get();
        $result = [];
        $i = 0;
        foreach($assigneds as $assigned){

            $names = '';
            $committeeMembers = Committee::where('id',$assigned->committee_id)
                ->with('members')
                ->get();
//                   return $committeeMembers;
            foreach ($committeeMembers as $committeeMember){

                $isOpen = $committeeMember->status;
                $result[$i]['title']=$committeeMember->title;
                foreach ($committeeMember->members as $member){
                    $names.= $member->user->full_name." , ";

                    if(Auth::user()->id == $member->user->id)
                        $isAssigned = true;

                    if($isOpen == "OPEN" && Auth::user()->id == $member->user->id){
                        $isSubmit = true;
                    }
                }
                $result[$i]['names'] = $names;
                $i++;
            }

        }
        $isMy = false;
        $authUser = Auth::user() ;
        if($authUser->id == $complaint->user_id) $isMy = true;

        if($isMy == "true" || $isAssigned ||
            ($authUser->department_code == $complaint->department_code && $authUser->role=="HEAD") ||
            ($authUser->department_code == $complaint->department_code && $complaint->privacy=="PUBLIC")

        ){
            return view('complaint.complaint_detail')->with([
                'complaint'=> $complaint,
                'user' => $user,
                'category' => $category,
                'committees' => $committees,
                'assigned' =>$result,
                'isSubmitReport' => $isSubmit,
                'isAssigned' => $isAssigned
            ]);
        }
            else
            {
                return response(view('errors.access-denied'));
            }

    }
    public function  showPendingComplaint(){

        $user = Auth::user();
        $complaints = Complaint::with('user')->orderBy('created_at','desc')
            ->where('status','=','PENDING')
            ->where('department_code','=',$user->department_code)
            ->get();

        return view('complaint.complaint_index')->with(['complaints'=>$complaints,'page_title'=>'Pending Complaints']);
    }

    public function postAssignCommittee(Request $request){

        $committee_title = Committee::find(Input::get('committee_id'))->title;
        try{
            AssignedCommittee::create(
                [
                    'complaint_id' => Input::get('complaint_id'),
                    'committee_id' => Input::get('committee_id'),
                    'department_code' => Auth::user()->department_code
                ]
            );

            $complaint = Complaint::find(Input::get('complaint_id'));
            $complaint->status = "IN-PROGRESS";
            $complaint->save();

            $message = "\"".$committee_title ."\" has been assigned to this complaint successfully!!";
            $request->session()->flash('success', $message);
            return redirect::back();

        }
        catch (QueryException $queryException){

            $request->session()->flash('error','This Committee is already assigned to this complaint.');
            return redirect::back();
        }
    }

    public function rejectComplaint($id){

        $complaint = Complaint::find($id);

        if(Auth::user()->role =="HEAD" && $complaint->status =="PENDING"){

            $complaint->status = "REJECTED";
            $complaint->save();
            return redirect('complaint/pending');

        }
        else return response(view('errors.access-denied'));

    }

    public function cancelComplaint($id){

        $res[] = Validations::isValidated($id);
        if(!$res[0]['isMy']){

            return response(view('errors.access-denied'));
        }

        $complaint = Complaint::find($id);
        if($complaint->status != "PENDING")
            return "Opps! You tried to do something that is not supposed to do.";
        $complaint->status = "CANCELLED";
        $complaint->save();
        return redirect::back();
    }

    public function myCommitteeComplaints(){

        $myCommittees = MemberCommittee::where('user_id',Auth::user()->id)->get(['committee_id']);
        $x = [];
        foreach ($myCommittees as $committee){
            $x[] = $committee['committee_id'];
        }
        $committees = Committee::whereIn('id',$x)
                                ->where('status',"OPEN")
                                ->with('complaints')
                                ->with('members')
                                ->get();
        return view('complaint.my-committee-complaints')->with(['committees'=>$committees,'page_title'=>'Complaints assigned to my Committees']);
    }

    public function getSubmitReport(){

        $userId = Input::get('user');
        $complaintId = Input::get('complaint');
        $complaint = Complaint::find($complaintId);

        $res[] = Validations::isValidated($complaintId);

        if($res[0]['isSubmit']){

            return view('complaint.submit_complaint_report')->with(['userId'=>$userId,'complaintId'=>$complaintId,'complaint'=>$complaint]);
        }
        else   return response(view('errors.access-denied'));

    }
    public function postSubmitReport( Request $request){

        $complaintId = $request->get('complaintId');
        $res[] = Validations::isValidated($complaintId);

        if(!$res[0]['isSubmit']){

            return response(view('errors.access-denied'));
        }

        try{
            Report::create(
                [
                    'complaint_id' => $complaintId,
                    'user_id'=>Auth::user()->id,
                    'comment' => Input::get('reportBody')
                ]
            );

            $complaint = Complaint::find($complaintId);
            $complaint->status = 'CLOSED';
            $complaint->save();

        }catch(QueryException $e){
            return $e;
        }

        $request->session()->flash('success','Report has been successfully submitted.');
        return redirect::to('complaint/my-committee');

    }

}
