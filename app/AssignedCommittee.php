<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedCommittee extends Model
{
    //
    protected $table = "assigned_committee";
    protected $fillable= [
        'complaint_id',
        'committee_id',
        'department_code'
    ];


}
