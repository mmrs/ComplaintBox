<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{
    //
    protected $table = "committees";
    protected $fillable= [
        'title',
        'department_code',
        'status'
    ];

    public function members(){
        return $this->hasMany('\App\MemberCommittee');
    }

    public function user(){
        return $this->hasMany('\App\User');
    }

    public function complaints(){
        return $this->belongsToMany('\App\Complaint','assigned_committee','committee_id','complaint_id');
    }


}
