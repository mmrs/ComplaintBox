<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPenalize extends Model
{
    protected $table = "user_penalizes";

    protected $fillable= [
        'user_id',
        'complaint_id',
        'department_code',
        'penalize_date'
    ];
}
