<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplaintCategory extends Model
{
    //

    protected $table = "complaint_categories";
    protected $fillable = [
        'category_name',
        'display_name'
    ];

}
